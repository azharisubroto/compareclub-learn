<?php

	add_action( 'wp_enqueue_scripts', 'himmelen_enqueue_dynamic_styles', '999' );

	function himmelen_enqueue_dynamic_styles( ) {
        global $page;
		if( !is_singular('post') ) {
                    // remove later
                    global $himmelen_theme_options;

                    if ( function_exists( 'is_multisite' ) && is_multisite() ){
                        $cache_file_name = 'cache.skin.b' . get_current_blog_id();
                    } else {
                        $cache_file_name = 'cache.skin';
                    }

                    $ipanel_saved_date = get_option( 'ipanel_saved_date', 1 );
                    $cache_saved_date = get_option( 'cache_css_saved_date', 0 );

                    if( file_exists( get_stylesheet_directory() . '/cache/' . $cache_file_name . '.css' ) ) {
                        $cache_status = 'exist';

                        if($ipanel_saved_date > $cache_saved_date) {
                            $cache_status = 'no-exist';
                        }

                    } else {
                        $cache_status = 'no-exist';
                    }

                    if ( defined('DEMO_MODE') ) {
                        $cache_status = 'no-exist';
                    }

                    if ( $cache_status == 'exist' ) {
                        wp_register_style( $cache_file_name, get_stylesheet_directory_uri() . '/cache/' . $cache_file_name . '.css', array(), $cache_saved_date);
                        wp_enqueue_style( $cache_file_name );
                    } else {
                        
                        $out = '';

                        $generated = microtime(true);

                        $out = himmelen_get_css();
                
                        $out = str_replace( array( "\t", "
            ", "\n", "  ", "   ", ), array( "", "", " ", " ", " ", ), $out );

                        $out .= '/* CSS Generator Execution Time: ' . floatval( ( microtime(true) - $generated ) ) . ' seconds */';

                        $cache_file = @fopen( get_stylesheet_directory() . '/cache/' . $cache_file_name . '.css', 'w' );

                        if ( @fwrite( $cache_file, $out ) ) {
                        
                            wp_register_style( $cache_file_name, get_stylesheet_directory_uri() . '/cache/' . $cache_file_name . '.css', array(), $cache_saved_date);
                            wp_enqueue_style( $cache_file_name );

                            // Update save options date
                            $option_name = 'cache_css_saved_date';
                            
                            $new_value = microtime(true) ;

                            if ( get_option( $option_name ) !== false ) {

                                // The option already exists, so we just update it.
                                update_option( $option_name, $new_value );

                            } else {

                                // The option hasn't been added yet. We'll add it with $autoload set to 'no'.
                                $deprecated = null;
                                $autoload = 'no';
                                add_option( $option_name, $new_value, $deprecated, $autoload );
                            }
                        }
                    
                    }
        }
	}

	function himmelen_get_css () {
		global $himmelen_theme_options;
		// ===
		ob_start();
    ?>
    <?php
    if ( defined('DEMO_MODE') && isset($_GET['header_height']) ) {
      $himmelen_theme_options['header_height'] = $_GET['header_height'];
    }

    if(isset($himmelen_theme_options['header_height']) && $himmelen_theme_options['header_height'] > 0) {
        $header_height = $himmelen_theme_options['header_height'];
    } else {
        $header_height = 144;
    } 
    ?>
    header .col-md-12 {
        height: <?php echo intval($header_height); ?>px;
    }
    <?php
    // Retina logo
    if(isset($himmelen_theme_options['logo_width']) && $himmelen_theme_options['logo_width'] > 0) {
        $logo_width = $himmelen_theme_options['logo_width'];
    } else {
        $logo_width = 260;
    } 
    ?>
    header .logo-link img {
        width: <?php echo intval($logo_width); ?>px;
    }
    <?php 
    /**
    * Custom CSS
    **/
    if(isset($himmelen_theme_options['custom_css_code'])) { 

        echo $himmelen_theme_options['custom_css_code']; // This variable contains user Custom CSS code and can't be escaped with WordPress functions 

    } ?>
    
    /** 
    * Theme Google Font
    **/
    <?php 
        // Demo settings
        if ( defined('DEMO_MODE') && isset($_GET['header_font']) ) {
          $himmelen_theme_options['header_font']['font-family'] = $_GET['header_font'];
        }
        if ( defined('DEMO_MODE') && isset($_GET['body_font']) ) {
          $himmelen_theme_options['body_font']['font-family'] = $_GET['body_font'];
        }
        if ( defined('DEMO_MODE') && isset($_GET['additional_font']) ) {
          $himmelen_theme_options['additional_font']['font-family'] = $_GET['additional_font'];
        }

        if(isset($himmelen_theme_options['font_google_disable']) && $himmelen_theme_options['font_google_disable']) {

            $himmelen_theme_options['body_font']['font-family'] = $himmelen_theme_options['font_regular'];
            $himmelen_theme_options['header_font']['font-family'] = $himmelen_theme_options['font_regular'];
            $himmelen_theme_options['additional_font']['font-family'] = $himmelen_theme_options['font_regular'];
        }
    ?>
    h1, h2, h3, h4, h5, h6 {
        font-family: '<?php echo esc_html($himmelen_theme_options['header_font']['font-family']); ?>';
    }
    blockquote,
    header .header-blog-info,
    .author-bio strong,
    .blog-post-related-single .blog-post-related-title,
    .blog-post-related-item .blog-post-related-title,
    .navigation-post .nav-post-name {
        font-family: '<?php echo esc_html($himmelen_theme_options['header_font']['font-family']); ?>';
    }
    h1 {
        font-size: <?php echo esc_html($himmelen_theme_options['header_font']['font-size']); ?>px;
    }
    body {
        font-family: '<?php echo esc_html($himmelen_theme_options['body_font']['font-family']); ?>';
        font-size: <?php echo esc_html($himmelen_theme_options['body_font']['font-size']); ?>px;
    }
    <?php if(isset($himmelen_theme_options['additional_font_enable']) && $himmelen_theme_options['additional_font_enable']): ?>
    .navbar .nav > li a,
    .navbar .navbar-toggle,
    .blog-post .post-info,
    a.btn,
    .btn,
    .btn:focus,
    input[type="submit"],
    .woocommerce #content input.button, 
    .woocommerce #respond input#submit, 
    .woocommerce a.button, 
    .woocommerce button.button,
    .woocommerce input.button, 
    .woocommerce-page #content input.button, 
    .woocommerce-page #respond input#submit, 
    .woocommerce-page a.button, 
    .woocommerce-page button.button, 
    .woocommerce-page input.button, 
    .woocommerce a.added_to_cart, 
    .woocommerce-page a.added_to_cart,
    a.more-link,
    .footer-sidebar.sidebar .widgettitle,
    .footer-sidebar-2.sidebar .widgettitle,
    .blog-post .post-info-date,
    .blog-post .post-categories,
    .blog-post-related.blog-post-related-loop .blog-post-related-item .blog-post-related-date,
    .page-item-title-single .post-date,
    .page-item-title-single .post-categories,
    .author-bio h5,
    .comment-metadata .author,
    .comment-metadata .date,
    .blog-post-related-single .post-categories,
    .blog-post-related-single .blog-post-related-date,
    .homepage-welcome-block h5,
    .sidebar .widget.widget_himmelen_text .himmelen-textwidget h5,
    .page-item-title-archive p,
    .navigation-post .nav-post-title,
    .navigation-paging.navigation-post a,
    .himmelen-popular-post-list-wrapper .himmelen-popular-post .himmelen-popular-post-category,
    .himmelen-popular-post-list-wrapper .himmelen-popular-post .himmelen-popular-post-date,
    .himmelen-post-list .himmelen-post-details .himmelen-post-category,
    .himmelen-post-pagination .himmelen-post-pagination-category,
    .blog-post .entry-content h5,
    .page .entry-content h5 {
        font-family: '<?php echo esc_html($himmelen_theme_options['additional_font']['font-family']); ?>';
    }
    <?php endif; ?>
    
    /**
    * Colors and color skins
    */
    <?php
    // Demo settings
    if ( defined('DEMO_MODE') && isset($_GET['color_skin_name']) ) {
      $himmelen_theme_options['color_skin_name'] = $_GET['color_skin_name'];
    }

    if(!isset($himmelen_theme_options['color_skin_name'])) {
        $color_skin_name = 'none';
    }
    else {
        $color_skin_name = $himmelen_theme_options['color_skin_name'];
    }
    // Use panel settings
    if($color_skin_name == 'none') {

        $theme_body_color = $himmelen_theme_options['theme_body_color'];
        $theme_text_color = $himmelen_theme_options['theme_text_color'];
        $theme_main_color = $himmelen_theme_options['theme_main_color'];
        $theme_header_bg_color = $himmelen_theme_options['theme_header_bg_color'];
        $theme_cat_menu_bg_color = $himmelen_theme_options['theme_cat_menu_bg_color'];
        $theme_footer_color = $himmelen_theme_options['theme_footer_color'];
        $theme_masonry_bg_color = $himmelen_theme_options['theme_masonry_bg_color'];

    }
    // Default skin
    if($color_skin_name == 'default') {
        
        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#9BA78A';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Black skin
    if($color_skin_name == 'black') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#444444';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#1b1b1b';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Light blue skin
    if($color_skin_name == 'lightblue') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#A2C6EA';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Blue skin
    if($color_skin_name == 'blue') {
        
        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#346DF4';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Red
    if($color_skin_name == 'red') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#e86f75';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Green
    if($color_skin_name == 'green') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#00BC8F';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Magnium
    if($color_skin_name == 'magnium') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#4686cc';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Fencer
    if($color_skin_name == 'fencer') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#26cdb3';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Perfectum
    if($color_skin_name == 'perfectum') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#F2532F';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Simplegreat
    if($color_skin_name == 'simplegreat') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#C3A36B';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';

    }
    // Himmelen Light
    if($color_skin_name == 'piemont') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#ec9f2e';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#262626';
        $theme_masonry_bg_color = '#F8F8F8';

    }
    // Simple
    if($color_skin_name == 'grey') {

        $theme_body_color = '#F7F7F7';
        $theme_text_color = '#000000';
        $theme_main_color = '#8e9da5';
        $theme_header_bg_color = '#FFFFFF';
        $theme_cat_menu_bg_color = '#FFFFFF';
        $theme_footer_color = '#3C3D41';
        $theme_masonry_bg_color = '#FFFFFF';
    }
    ?>
    <?php if(isset($himmelen_theme_options['body_bg_image']) && $himmelen_theme_options['body_bg_image']['url'] <> ''): ?>
    .st-sidebar-pusher {
        <?php 
        echo 'background-image: url('.esc_url($himmelen_theme_options['body_bg_image']['url']).');';
        if(isset($himmelen_theme_options['body_bg_style'])) {
            if($himmelen_theme_options['body_bg_style'] == 'repeat') {
                echo 'background-repeat: repeat;';
            }
            if($himmelen_theme_options['body_bg_style'] == 'cover') {
                echo 'background-size: cover;';
            }
        }
        ?>
    }
    <?php endif; ?>
    <?php if(isset($himmelen_theme_options['header_bg_image']) && $himmelen_theme_options['header_bg_image']['url'] <> ''): ?>
    header {
        <?php 
        
        echo 'background-image: url('.esc_url($himmelen_theme_options['header_bg_image']['url']).');';
        if(isset($himmelen_theme_options['header_bg_style'])) {

            if($himmelen_theme_options['header_bg_style'] == 'repeat') {
                echo 'background-repeat: repeat;';
            }
            if($himmelen_theme_options['header_bg_style'] == 'cover') {
                echo 'background-size: cover;';
            }
        }
        
        ?>
    }
    .mainmenu-belowheader:not(.fixed),
    .mainmenu-belowheader:not(.fixed) .navbar {
        background-color: transparent!important;
    }
    <?php endif; ?>
    body {
        background-color: <?php echo esc_html($theme_body_color); ?>;
        color: <?php echo esc_html($theme_text_color); ?>;
    }
    .st-pusher, 
    .st-sidebar-pusher,
    .st-sidebar-menu .sidebar {
        background-color: <?php echo esc_html($theme_body_color); ?>;
    }
    .woocommerce #content input.button.alt,
    .woocommerce #respond input#submit.alt,
    .woocommerce a.button.alt,
    .woocommerce button.button.alt,
    .woocommerce input.button.alt,
    .woocommerce-page #content input.button.alt,
    .woocommerce-page #respond input#submit.alt,
    .woocommerce-page a.button.alt,
    .woocommerce-page button.button.alt,
    .woocommerce-page input.button.alt,
    .btn.alt,
    .nav > li .sub-menu,
    .blog-post .tags a:hover,
    .blog-post-related-item-details,
    .blog-post-related.blog-post-related-loop .blog-post-related-item .blog-post-related-image.blog-post-related-no-image .blog-post-related-item-inside,
    .blog-post-related.blog-post-related-loop .blog-post-related-item:hover .blog-post-related-item-inside,
    .blog-post .sticky-post-badge,
    .navigation-post a:hover,
    #top-link,
    .sidebar .widget_calendar th,
    .sidebar .widget_calendar tfoot td,
    .sidebar .widget_tag_cloud .tagcloud a:hover,
    .sidebar .widget_product_tag_cloud .tagcloud a:hover,
    .comment-meta .reply a:hover,
    .himmelen-post-list .himmelen-post-details .himmelen-read-more a:hover,
    .himmelen-post-wrapper-style-2,
    .himmelen-post-wrapper-style-2 .himmelen-post-list .himmelen-post-details-wrapper,
    .himmelen-popular-post-list-wrapper .himmelen-popular-post.himmelen-popular-post-small,
    .sidebar .widget .widget-social-follow a,
    .homepage-welcome-block-content-left,
    .homepage-welcome-block-content-right,
    body .owl-theme .owl-controls .owl-page.active span, 
    body .owl-theme .owl-controls.clickable .owl-page:hover span,
    .st-sidebar-menu-close-btn {
        background-color: <?php echo esc_html($theme_main_color); ?>;
    }
    a,
    a:focus,
    blockquote:before,
    .navbar .nav > li > a:hover,
    .social-icons-wrapper a:hover,
    .blog-post .format-quote .entry-content:before,
    .blog-post .post-categories,
    .blog-post .post-categories a,
    .blog-post .post-header-title sup,
    .blog-post .post-header-title a:hover,
    .blog-post .post-info > div a:hover,
    .author-bio .author-social-icons li a:hover,
    .post-social a:hover,
    .navigation-paging.navigation-post a,
    .navigation-post .nav-post-title,
    .footer-sidebar-2.sidebar .widget a:hover,
    footer a:hover,
    .sidebar .widget ul > li a:hover,
    .sidebar .widget_text a,
    .comment-metadata .author a,
    .comment-metadata .date a:hover,
    .himmelen-post-pagination .himmelen-post-pagination-item:hover .himmelen-post-pagination-title,
    .himmelen-post-pagination .himmelen-post-pagination-item.active .himmelen-post-pagination-title,
    .himmelen-popular-post-list-wrapper .himmelen-popular-post .himmelen-popular-post-title a:hover,
    body .select2-results .select2-highlighted {
        color: <?php echo esc_html($theme_main_color); ?>;
    }
    .woocommerce #content input.button.alt,
    .woocommerce #respond input#submit.alt,
    .woocommerce a.button.alt,
    .woocommerce button.button.alt,
    .woocommerce input.button.alt,
    .woocommerce-page #content input.button.alt,
    .woocommerce-page #respond input#submit.alt,
    .woocommerce-page a.button.alt,
    .woocommerce-page button.button.alt,
    .woocommerce-page input.button.alt,
    .btn.alt,
    .navbar .nav > li > a:hover,
    .social-icons-wrapper a:hover,
    .sidebar .widget_calendar tbody td a,
    .himmelen-post-list .himmelen-post-details .himmelen-read-more a:hover,
    .himmelen-post-wrapper-style-2 .himmelen-post-list-nav .himmelen-post-list-nav-prev,
    .himmelen-post-wrapper-style-2 .himmelen-post-list-nav .himmelen-post-list-nav-next {
        border-color: <?php echo esc_html($theme_main_color); ?>;
    }
    header {
        background-color: <?php echo esc_html($theme_header_bg_color); ?>;
    }
    .mainmenu-belowheader {
        background-color: <?php echo esc_html($theme_cat_menu_bg_color); ?>;
    }
    .container-fluid-footer {
        background-color: <?php echo esc_html($theme_footer_color); ?>;
    }
    .blog-masonry-layout .blog-post.content-block .post-content,
    .blog-post-list-layout.blog-post .post-content {
        background-color: <?php echo esc_html($theme_masonry_bg_color); ?>;
    }
    
    <?php

    	$out = ob_get_clean();

		$out .= ' /*' . date("Y-m-d H:i") . '*/';
		/* RETURN */
		return $out;
	}
?>
