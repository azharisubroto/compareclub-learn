<?php
/** 
 * Plugin recomendations
 **/
global $himmelen_theme_options;

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

require_once ('class-tgm-plugin-activation.php');

add_action( 'tgmpa_register', 'himmelen_register_required_plugins' );

function himmelen_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        array(
            'name'                  => 'Himmelen Custom Metaboxes', // The plugin name
            'slug'                  => 'cmb2', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/cmb2.zip', // The plugin source
            'required'              => true, // If false, the plugin is only 'recommended' instead of required
            'version'               => '2.3.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name'                  => 'Himmelen Translation Manager', // The plugin name
            'slug'                  => 'loco-translate', // The plugin slug (typically the folder name)
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
        ),
        array(
            'name'                  => 'Instagram Feed', // The plugin name
            'slug'                  => 'instagram-feed', // The plugin slug (typically the folder name)
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
        ),
        array(
            'name'                  => 'MailChimp for WordPress (Newsletter signup)', // The plugin name
            'slug'                  => 'mailchimp-for-wp', // The plugin slug (typically the folder name)
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
        ),
        array(
            'name'                  => 'WordPress LightBox', // The plugin name
            'slug'                  => 'responsive-lightbox', // The plugin slug (typically the folder name)
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
        ),
        array(
            'name'                  => 'Contact Form 7', // The plugin name
            'slug'                  => 'contact-form-7', // The plugin slug (typically the folder name)
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
        ),
        array(
            'name'                  => 'Regenerate Thumbnails', // The plugin name
            'slug'                  => 'regenerate-thumbnails', // The plugin slug (typically the folder name)
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
        ),
        array(
            'name'                  => 'WP Retina 2x', // The plugin name
            'slug'                  => 'wp-retina-2x', // The plugin slug (typically the folder name)
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
        )

    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'domain'            => 'himmelen',           // Text domain - likely want to be the same as your theme.
        'default_path'      => '',                          // Default absolute path to pre-packaged plugins
        'menu'              => 'install-required-plugins',  // Menu slug
        'has_notices'       => true,                        // Show admin notices or not
        'dismissable'  => true,
        'is_automatic'      => true,                       // Automatically activate plugins after installation or not
        'message'           => '',                          // Message to output right before the plugins table
        'strings'           => array(
            'page_title'                                => __( 'Install Required Plugins', 'himmelen' ),
            'menu_title'                                => __( 'Install Plugins', 'himmelen' ),
            'installing'                                => __( 'Installing Plugin: %s', 'himmelen' ), // %1$s = plugin name
            'oops'                                      => __( 'Something went wrong with the plugin API.', 'himmelen' ),
            'notice_can_install_required'               => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' , 'himmelen' ), // %1$s = plugin name(s)
            'notice_can_install_recommended'            => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'himmelen' ), // %1$s = plugin name(s)
            'notice_cannot_install'                     => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'himmelen' ), // %1$s = plugin name(s)
            'notice_can_activate_required'              => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'himmelen' ), // %1$s = plugin name(s)
            'notice_can_activate_recommended'           => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'himmelen' ), // %1$s = plugin name(s)
            'notice_cannot_activate'                    => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'himmelen' ), // %1$s = plugin name(s)
            'notice_ask_to_update'                      => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'himmelen' ), // %1$s = plugin name(s)
            'notice_cannot_update'                      => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'himmelen' ), // %1$s = plugin name(s)
            'install_link'                              => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'himmelen' ),
            'activate_link'                             => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'himmelen'),
            'return'                                    => __( 'Return to Required Plugins Installer', 'himmelen' ),
            'plugin_activated'                          => __( 'Plugin activated successfully.', 'himmelen' ),
            'complete'                                  => __( 'All plugins installed and activated successfully. %s', 'himmelen' ), // %1$s = dashboard link
            'nag_type'                                  => 'updated' // Determines admin notice type - can only be 'updated' or 'error'
        )
    );

    tgmpa( $plugins, $config );

}

// Customisation Menu Links
class himmelen_description_walker extends Walker_Nav_Menu{
      function start_el(&$output, $item, $depth = 0, $args = Array(), $current_object_id = 0 ){
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
           $class_names = $value = '';
           $classes = empty( $item->classes ) ? array() : (array) $item->classes;
           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
          
           $add_class = '';
           
           $post = get_post($item->object_id);          

               $class_names = ' class="'.$add_class.' '. esc_attr( $class_names ) . '"';
               $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
               $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
               $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
               $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

                    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

                if (is_object($args)) {
                    $item_output = $args->before;
                    $item_output .= '<a'. $attributes .'>';
                    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID );
                    $item_output .= $args->link_after;
                    if($item->description !== '') {
                        $item_output .= '<span>'.$item->description.'</span>';
                    }
                    
                    $item_output .= '</a>';
                    $item_output .= $args->after;
                    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

                }
     }
}

function himmelen_google_fonts_url() {

    global $himmelen_theme_options;

    $font_url = '';
    $font_header = '';
    $font_body = '';
    $font_additional = '';

    // Demo settings
    if ( defined('DEMO_MODE') && isset($_GET['header_font']) ) {
      $himmelen_theme_options['header_font']['font-family'] = $_GET['header_font'];
    }
    if ( defined('DEMO_MODE') && isset($_GET['body_font']) ) {
      $himmelen_theme_options['body_font']['font-family'] = $_GET['body_font'];
    }
    if ( defined('DEMO_MODE') && isset($_GET['additional_font']) ) {
      $himmelen_theme_options['additional_font']['font-family'] = $_GET['additional_font'];
    }

    if(!isset($himmelen_theme_options['font_google_disable']) || $himmelen_theme_options['font_google_disable'] == false) {

        // Header font
        if(isset($himmelen_theme_options['header_font'])) {
            $font_header = $himmelen_theme_options['header_font']['font-family'];

            if(isset($himmelen_theme_options['header_font_options'])) {
                $font_header = $font_header.':'.$himmelen_theme_options['header_font_options'];
            }
        }
        // Body font
        if(isset($himmelen_theme_options['body_font'])) {
            $font_body = '|'.$himmelen_theme_options['body_font']['font-family'];

            if(isset($himmelen_theme_options['body_font_options'])) {
                $font_body = $font_body.':'.$himmelen_theme_options['body_font_options'];
            }
        }
        // Additional font
        if(isset($himmelen_theme_options['additional_font_enable']) && $himmelen_theme_options['additional_font_enable']) {
            if(isset($himmelen_theme_options['additional_font'])) {
                $font_additional = '|'.$himmelen_theme_options['additional_font']['font-family'].'|';
            }
        }

        // Build Google Fonts request
        $font_url = add_query_arg( 'family', urlencode( $font_header.$font_body.$font_additional ), "//fonts.googleapis.com/css" );

    }
    
    return $font_url;
}

// Custom layout functions
function himmelen_header_promo_show() {
    global $himmelen_theme_options;

    echo '<div class="header-promo-content">'.($himmelen_theme_options['header_banner_editor']).'</div>';
}

function himmelen_logo_show() {
    global $himmelen_theme_options;
    ?>
    <div class="logo">
    <a class="logo-link" href="<?php echo home_url(); ?>"><img src="<?php echo get_header_image(); ?>" alt="<?php bloginfo('name'); ?>"></a>
    <?php if(get_bloginfo('description')!=='') {
      echo '<div class="header-blog-info">';
      if(isset($himmelen_theme_options['disable_header_tagline']) && !$himmelen_theme_options['disable_header_tagline']) {
        bloginfo('description');
      }
      echo '</div>';
    }
    ?>
    </div>
    <?php
}

function himmelen_menu_below_header_show() {
    global $himmelen_theme_options;

    // MainMenu styles
    $menu_add_class = '';

    if(isset($himmelen_theme_options['header_menu_font_decoration'])) {
        $menu_add_class .= ' mainmenu-'.esc_html($himmelen_theme_options['header_menu_font_decoration']);
    }
    if(isset($himmelen_theme_options['header_menu_font_size'])) {
        $menu_add_class .= ' mainmenu-'.esc_html($himmelen_theme_options['header_menu_font_size']);
    }
    if(isset($himmelen_theme_options['header_menu_arrow_style'])) {
        $menu_add_class .= ' mainmenu-'.esc_html($himmelen_theme_options['header_menu_arrow_style']);
    }
    
    // Center menu
    $menu_add_class .= ' menu-center';

    if(isset($himmelen_theme_options['enable_sticky_header']) && $himmelen_theme_options['enable_sticky_header']) {

      $menu_add_class .= ' sticky-header';

    }

    ?>

    <?php
    // Main Menu

    $menu = wp_nav_menu(
        array (
            'theme_location'  => 'primary',
            'echo' => FALSE,
            'fallback_cb' => '__return_false'
        )
    );

    if (!empty($menu)):
 
    ?>
    <div class="mainmenu-belowheader<?php echo esc_attr($menu_add_class); ?> clearfix">
        
        <div id="navbar" class="navbar navbar-default clearfix">
          
          <div class="navbar-inner">
              <div class="container">
             
                  <div class="navbar-toggle" data-toggle="collapse" data-target=".collapse">
                    <?php _e( 'Menu', 'himmelen' ); ?>
                  </div>
                  <div class="navbar-left-wrapper">
                    <?php // Demo settings
                    if ( defined('DEMO_MODE') && isset($_GET['enable_offcanvas_sidebar']) ) {
                      $himmelen_theme_options['enable_offcanvas_sidebar'] = $_GET['enable_offcanvas_sidebar'];
                    }

                    ?>
                    <ul class="header-nav">
                        <?php
                            if(isset($himmelen_theme_options['enable_offcanvas_sidebar'])&&($himmelen_theme_options['enable_offcanvas_sidebar'])): 
                        ?>
                        <li class="float-sidebar-toggle"><div id="st-sidebar-trigger-effects"><a class="float-sidebar-toggle-btn" data-effect="st-sidebar-effect-2"><i class="fa fa-align-left"></i></a></div></li>
                        <?php endif; ?>
                    </ul>
                  </div>
                  <div class="navbar-center-wrapper">
                  <?php
                 
                     wp_nav_menu(array(
                      'theme_location'  => 'primary',
                      'container_class' => 'navbar-collapse collapse',
                      'menu_class'      => 'nav',
                      'walker'          => new himmelen_description_walker
                      ));  
                  
                  ?>
                  </div>
                  <div class="navbar-right-wrapper">
                    <div class="search-bar-header">
                      <?php
                      if(isset($himmelen_theme_options['disable_top_menu_search']) && !$himmelen_theme_options['disable_top_menu_search']) {
                        echo get_search_form();
                      }
                      ?>
                      <div class="search-bar-header-close-btn">×</div>
                    </div>
                  </div>
              </div>
          </div>
          
        </div>
       
    </div>
    <?php endif; ?>
    
    <?php
    // MainMenu Below header position END 
}

function himmelen_social_show($showtitles = false) {
    global $himmelen_theme_options;

    $social_services_arr['facebook'] = 'Facebook';
    $social_services_arr['vk'] = 'VKontakte';
    $social_services_arr['twitter'] = 'Twitter';
    $social_services_arr['google-plus'] = 'Google+';
    $social_services_arr['linkedin'] = 'LinkedIn';
    $social_services_arr['dribbble'] = 'Dribbble';
    $social_services_arr['behance'] = 'Behance';
    $social_services_arr['instagram'] = 'Instagram';
    $social_services_arr['tumblr'] = 'Tumblr';
    $social_services_arr['pinterest'] = 'Pinterest';
    $social_services_arr['vimeo-square'] = 'Vimeo';
    $social_services_arr['youtube'] = 'YouTube';
    $social_services_arr['skype'] = 'Skype';
    $social_services_arr['flickr'] = 'Flickr';
    $social_services_arr['rss'] = 'RSS';
    $social_services_arr['deviantart'] = 'Deviantart';
    $social_services_arr['500px'] = '500px';
    $social_services_arr['etsy'] = 'Etsy';
    $social_services_arr['telegram'] = 'Telegram';
    $social_services_arr['odnoklassniki'] = 'Odnoklassniki';
    $social_services_arr['houzz'] = 'Houzz';
    $social_services_arr['slack'] = 'Slack';
    $social_services_arr['qq'] = 'QQ';

    $s_count = 0;

    $social_services_html = '';

    foreach( $social_services_arr as $ss_data => $ss_value ){
      if(isset($himmelen_theme_options[$ss_data]) && (trim($himmelen_theme_options[$ss_data])) <> '') {
        $s_count++;
        $social_service_url = $himmelen_theme_options[$ss_data];
        $social_service = $ss_data;

        if($showtitles) {
            $social_title = '<span>'.$ss_value.'</span>';
        } else {
            $social_title = '';
        }

        $social_services_html .= '<a href="'.esc_url($social_service_url).'" target="_blank" class="a-'.esc_attr($social_service).'"><i class="fa fa-'.esc_attr($social_service).'"></i>'.wp_kses_post($social_title).'</a>';
      }
    }

    if($social_services_html <> '') {
      echo wp_kses_post('<div class="social-icons-wrapper">'.$social_services_html.'</div>');
    }
}

function himmelen_header_left_show() {
    global $himmelen_theme_options;

    // Show header banner
    if((isset($himmelen_theme_options['header_banner_editor'])) && ($himmelen_theme_options['header_banner_editor'] <> '') && (isset($himmelen_theme_options['header_banner_position'])) && ($himmelen_theme_options['header_banner_position'] == 'left')){
        himmelen_header_promo_show();
    }

    if((isset($himmelen_theme_options['header_logo_position'])) && ($himmelen_theme_options['header_logo_position'] == 'left')) {
        himmelen_logo_show();
    }

    if((isset($himmelen_theme_options['header_social_position'])) && ($himmelen_theme_options['header_social_position'] == 'left')) {
        himmelen_social_show();
    }           

}

function himmelen_header_center_show() {
    global $himmelen_theme_options;

    // Show header banner
    if((isset($himmelen_theme_options['header_banner_editor'])) && ($himmelen_theme_options['header_banner_editor'] <> '') && (isset($himmelen_theme_options['header_banner_position'])) && ($himmelen_theme_options['header_banner_position'] == 'center')){
        himmelen_header_promo_show();
    }

    if((isset($himmelen_theme_options['header_logo_position'])) && ($himmelen_theme_options['header_logo_position'] == 'center')) {
        himmelen_logo_show();
    }

    if((isset($himmelen_theme_options['header_social_position'])) && ($himmelen_theme_options['header_social_position'] == 'center')) {
        himmelen_social_show();
    }
}

function himmelen_header_right_show() {
    global $himmelen_theme_options;

    if((isset($himmelen_theme_options['header_logo_position'])) && ($himmelen_theme_options['header_logo_position'] == 'right')) {
        himmelen_logo_show();
    }

    // Show header banner
    if((isset($himmelen_theme_options['header_banner_editor'])) && ($himmelen_theme_options['header_banner_editor'] <> '') && (isset($himmelen_theme_options['header_banner_position'])) && ($himmelen_theme_options['header_banner_position'] == 'right')){
        himmelen_header_promo_show();
    }

    if((isset($himmelen_theme_options['header_social_position'])) && ($himmelen_theme_options['header_social_position'] == 'right')) {
        himmelen_social_show();
    }

    ?>
<?php
}

function himmelen_blog_slider_show() {

    global $himmelen_theme_options, $post;

    // Demo settings
    if ( defined('DEMO_MODE') && isset($_GET['blog_homepage_slider_style']) ) {
      $himmelen_theme_options['blog_homepage_slider_style'] = $_GET['blog_homepage_slider_style'];
    }
    if ( defined('DEMO_MODE') && isset($_GET['blog_homepage_slider_fullwidth']) ) {
      if($_GET['blog_homepage_slider_fullwidth'] == 1) {
        $himmelen_theme_options['blog_homepage_slider_fullwidth'] = true;
      }
      if($_GET['blog_homepage_slider_fullwidth'] == 0) {
        $himmelen_theme_options['blog_homepage_slider_fullwidth'] = false;
      }
    }

    if(isset($himmelen_theme_options['blog_homepage_slider_style'])) {
        $slider_style = $himmelen_theme_options['blog_homepage_slider_style'];
    } else {
        $slider_style = 2;
    }

    if($slider_style == 2) {
        $posts_per_page = 5;
    } else {
        $posts_per_page = 0;
    }

    $args = array(
        'posts_per_page'   => $posts_per_page,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'meta_key'         => '_post_featured_value',
        'meta_value'         => 'on',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => 0 
    );

    $posts = get_posts( $args );

    $total_posts = sizeof($posts);

    if($total_posts > 0) {

        if(isset($himmelen_theme_options['blog_homepage_slider_autoplay'])) {
            $slider_autoplay = $himmelen_theme_options['blog_homepage_slider_autoplay'];
        } else {
            $slider_autoplay = 1;
        }

        if($slider_autoplay == 1) {
            $slider_autoplay_class = ' autoplay ';
        } else {
            $slider_autoplay_class = ' ';
        }

        if(isset($himmelen_theme_options['blog_homepage_slider_navigation'])) {
            $slider_navigation = $himmelen_theme_options['blog_homepage_slider_navigation'];
        } else {
            $slider_navigation = 1;
        }
        if(isset($himmelen_theme_options['blog_homepage_slider_pagination'])) {
            $slider_pagination = $himmelen_theme_options['blog_homepage_slider_pagination'];
        } else {
            $slider_pagination = 1;
        }

        if($slider_autoplay == 1) {
            $slider_autoplay = 'true';
        } else {
            $slider_autoplay = 'false';
        }
        if($slider_navigation == 1) {
            $slider_navigation = 'true';
        } else {
            $slider_navigation = 'false';
        }
        if($slider_pagination == 1) {
            $slider_pagination = 'true';
        } else {
            $slider_pagination = 'false';
        }

        // Simple Slider
        if($slider_style == 1) {
            $style = ' style="display: none;"';
        } else {
            $style = '';
        }

        echo '<div class="himmelen-post-list-wrapper himmelen-post-wrapper-style-'.esc_attr($slider_style).''.esc_attr($slider_autoplay_class).'clearfix">';
        
        if(($total_posts > 1)&&($slider_navigation == 'true')) {
            echo '<div class="himmelen-post-list-nav"><div class="himmelen-post-list-nav-prev"></div><div class="himmelen-post-list-nav-next"></div></div>';
        }
        
        echo '<div id="himmelen-post-list" class="himmelen-post-list himmelen-post-style-'.$slider_style.'"'.$style.'>';

        $i = 0;

        $html_slider_posts_pagination = '';

        foreach($posts as $post) {

            setup_postdata($post);

            $limit = 20;

            $excerpt = explode(' ', get_the_excerpt(), $limit);
            if (count($excerpt)>=$limit) {
                array_pop($excerpt);
                $excerpt = implode(" ",$excerpt).'...';
            } else {
                $excerpt = implode(" ",$excerpt);
            } 

            $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);

            if((isset($himmelen_theme_options['blog_homepage_slider_fullwidth'])&&$himmelen_theme_options['blog_homepage_slider_fullwidth']) && (isset($himmelen_theme_options['blog_homepage_slider_style'])&&$himmelen_theme_options['blog_homepage_slider_style'] < 3)) {
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
            } else {
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-thumb');
            }

            if(has_post_thumbnail( $post->ID )) {
                $image_bg ='background-image: url('.$image[0].');';
            }
            else {
                $image_bg = '';
            }

            $categories_list = get_the_category_list( ', ' ); // This variable is Safe and does not need esc functions

            echo '<div class="himmelen-post">';

            // Simple slider Layout
            if($slider_style == 1) {
                echo '<div class="himmelen-post-image" data-style="'.esc_attr($image_bg).'"><div class="himmelen-post-image-wrapper">
                <div class="himmelen-post-details">
                    <div class="himmelen-post-category">'.$categories_list.'</div>
                    <div class="himmelen-post-title"><a href="'.get_permalink($post->ID).'"><h2>'.esc_html($post->post_title).'</h2></a></div>
                    <div class="himmelen-post-description">'.esc_html($excerpt).'</div>
                    <div class="himmelen-read-more"><a class="btn" href="'.get_permalink($post->ID).'">'.__('Read more', 'himmelen').'</a></div>
                </div>
                </div></div>';
            }

            // Advanced slider Layout
            if($slider_style == 2) {

                echo '<a href="'.get_permalink($post->ID).'"><div class="himmelen-post-image" data-style="'.esc_attr($image_bg).'"></div></a>';
                echo '<div class="himmelen-post-details-wrapper">
                <div class="himmelen-post-details">
                    <div class="himmelen-post-category">'.$categories_list.'</div>
                    <div class="himmelen-post-title"><a href="'.get_permalink($post->ID).'"><h2>'.$post->post_title.'</h2></a></div>
                    <div class="himmelen-post-description">'.esc_html($excerpt).'</div>
                    <div class="himmelen-read-more"><a class="btn" href="'.get_permalink($post->ID).'">'.__('Read more', 'himmelen').'</a></div>
                </div>
                </div>';

                $html_slider_posts_pagination .= '<div class="himmelen-post-pagination-item" data-slide="'.esc_attr($i+1).'"><div class="himmelen-post-pagination-category">'.$categories_list.'</div><div class="himmelen-post-pagination-title">'.$post->post_title.'</div></div>';
            }

            echo '</div>';

            $i++;

        } 

        wp_reset_postdata();


        echo '</div>';
        echo '</div>';

        if($slider_pagination == 'true') {
            echo '<div class="himmelen-post-pagination clearfix">'.$html_slider_posts_pagination.'</div>'; // This variable already safe and all data inside correctly escaped
        }
       
        // Simple slider JS
        if($slider_style == 1) {
            // Slider items per row
            $slider_slides = 1;

            echo '<script>(function($){
            $(document).ready(function() {

                $("#himmelen-post-list").owlCarousel({
                    items: '.esc_js($slider_slides).',
                    itemsDesktop:   [1199,'.esc_js($slider_slides).'],
                    itemsDesktopSmall: [979,1],
                    itemsTablet: [768,1],
                    itemsMobile : [479,1],
                    autoPlay: '.esc_js($slider_autoplay).',
                    navigation: '.esc_js($slider_navigation).',
                    navigationText : false,
                    pagination: '.esc_js($slider_pagination).',
                    afterInit : function(elem){
                        $(this).css("display", "block");
                    }
                });
                
                $(".himmelen-post-list.himmelen-post-style-2 .himmelen-next-post").on("click", function(e){
                    $("#himmelen-post-list").trigger("owl.next");
                });

                $(".himmelen-post-list.himmelen-post-style-2 .himmelen-next-post.last").on("click", function(e){
                    $("#himmelen-post-list").trigger("owl.goTo(1)");
                });
                
            });})(jQuery);</script>';
        }
        
    }

        
}

function himmelen_blog_popular_posts_show() {

    global $himmelen_theme_options, $post;

    if(isset($himmelen_theme_options['blog_homepage_popular_posts_layout'])) {
        $popular_posts_layout = $himmelen_theme_options['blog_homepage_popular_posts_layout'];
    } else {
        $popular_posts_layout = 'masonry';
    }

    if(isset($himmelen_theme_options['blog_homepage_popular_posts_limit'])) {
        $popular_posts_limit = $himmelen_theme_options['blog_homepage_popular_posts_limit'];
    } else {
        $popular_posts_limit = 2;
    }

    if(isset($himmelen_theme_options['blog_homepage_popular_posts_category'])) {
        $popular_posts_category = $himmelen_theme_options['blog_homepage_popular_posts_category'];
    } else {
        $popular_posts_category = '';
    }

    if($popular_posts_limit == 2) {

        if($popular_posts_layout == 'masonry') {
            $posts_limit = 6;
        }
        if($popular_posts_layout == 'small') {
            $posts_limit = 8;
        }
        if($popular_posts_layout == 'large') {
            $posts_limit = 4;
        }

    } else {
        if($popular_posts_layout == 'masonry') {
            $posts_limit = 3;
        }
        if($popular_posts_layout == 'small') {
            $posts_limit = 4;
        }
        if($popular_posts_layout == 'large') {
            $posts_limit = 2;
        }
    }

    $args = array(
        'posts_per_page'   => $posts_limit,
        'order'            => 'DESC',
        'category_name'         => $popular_posts_category,
        'orderby' => 'meta_value',
        'meta_key'         => 'post_views_count',
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => 0 
    );

    $posts = get_posts( $args );

    $total_posts = sizeof($posts);

    if($total_posts > 0) {

        echo '<div class="himmelen-popular-post-list-wrapper clearfix">';

        if(isset($himmelen_theme_options['blog_homepage_popular_posts_title']) && $himmelen_theme_options['blog_homepage_popular_posts_title'] !== '') {
            echo '<h2>'.esc_html($himmelen_theme_options['blog_homepage_popular_posts_title']).'</h2>';
        }
        
        echo '<div id="himmelen-popular-post-list" class="himmelen-popular-post-list">';
        echo '<div class="row himmelen-popular-post-row">';

        $i = 0;

        foreach($posts as $post) {

            setup_postdata($post);
            
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-thumb');

            if(has_post_thumbnail( $post->ID )) {
                $image_bg ='background-image: url('.$image[0].');';
            }
            else {
                $image_bg = '';
            }

            $categories_list = get_the_category_list( ', ' );

            if($popular_posts_layout == 'masonry') {
                $i_divider = 3;
                $i_show_big = array(0, 4);
            }
            if($popular_posts_layout == 'small') {
                $i_divider = 4;
                $i_show_big = array(-1);
            }
            if($popular_posts_layout == 'large') {
                $i_divider = 2;
                $i_show_big = array(0, 1, 2, 3, 4, 5);
            }

            if($i == $i_divider) {
                echo '</div><div class="row himmelen-popular-post-row">';
            }

            // Masonry layout
            if( in_array($i, $i_show_big)) {

                $limit = 30;

                $excerpt = explode(' ', get_the_excerpt(), $limit);
                if (count($excerpt)>=$limit) {
                    array_pop($excerpt);
                    $excerpt = implode(" ",$excerpt).'...';
                } else {
                    $excerpt = implode(" ",$excerpt);
                } 

                $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);

                // Big post without image
                echo '<div class="col-md-6">';
                echo '<div class="himmelen-popular-post himmelen-popular-post-large">';
                
                echo '<div class="himmelen-popular-post-details">
                    <div class="himmelen-popular-post-category">'.$categories_list.'</div>
                    <div class="himmelen-popular-post-title"><a href="'.get_permalink($post->ID).'"><h2>'.esc_html($post->post_title).'</h2></a></div>
                    <div class="himmelen-popular-post-date">'.get_the_time( get_option( 'date_format' ), $post->ID ).'</div>
                    <div class="himmelen-popular-post-description">'.esc_html($excerpt).'</div>
                </div>';

                echo '</div>';
                echo '</div>';
    
            } else {

                // Small post with image
                echo '<div class="col-md-3">';
                echo '<div class="himmelen-popular-post himmelen-popular-post-small">';

                echo '<a href="'.get_permalink($post->ID).'"><div class="himmelen-popular-post-image" data-style="'.esc_attr($image_bg).'"></div></a>
                <div class="himmelen-popular-post-details">
                    <div class="himmelen-popular-post-category">'.$categories_list.'</div>
                    <div class="himmelen-popular-post-title"><a href="'.get_permalink($post->ID).'"><h2>'.esc_html($post->post_title).'</h2></a></div>
                    <div class="himmelen-popular-post-date">'.get_the_time( get_option( 'date_format' ), $post->ID ).'</div>
                </div>';

                echo '</div>';
                echo '</div>';

            }

            $i++;

        }

        wp_reset_postdata();

        echo '</div>';
        echo '</div>';

        echo '</div>';
    }

        
}

/* Homepage Welcome Block */
function himmelen_welcome_block_show() {

    global $himmelen_theme_options;

    $col_left_class = 'col-md-3';
    $col_center_class = 'col-md-12';
    $col_right_class = 'col-md-3';

    if((isset($himmelen_theme_options['blog_enable_homepage_welcome_block_left']) && $himmelen_theme_options['blog_enable_homepage_welcome_block_left'])&&(isset($himmelen_theme_options['blog_enable_homepage_welcome_block_right']) && $himmelen_theme_options['blog_enable_homepage_welcome_block_right'])) {
        $col_center_class = 'col-md-6';
    }

    if((isset($himmelen_theme_options['blog_enable_homepage_welcome_block_left']) && $himmelen_theme_options['blog_enable_homepage_welcome_block_left'])&&(isset($himmelen_theme_options['blog_enable_homepage_welcome_block_right']) && !$himmelen_theme_options['blog_enable_homepage_welcome_block_right'])) {
        $col_center_class = 'col-md-9';
    }

    if((isset($himmelen_theme_options['blog_enable_homepage_welcome_block_left']) && !$himmelen_theme_options['blog_enable_homepage_welcome_block_left'])&&(isset($himmelen_theme_options['blog_enable_homepage_welcome_block_right']) && $himmelen_theme_options['blog_enable_homepage_welcome_block_right'])) {
        $col_center_class = 'col-md-9';
    }

    echo '<div class="homepage-welcome-block">';

    echo '<div class="container">';
    echo '<div class="row">';
    
    if(isset($himmelen_theme_options['blog_enable_homepage_welcome_block_left']) && $himmelen_theme_options['blog_enable_homepage_welcome_block_left']) {
        echo '<div class="'.esc_attr($col_left_class).'">';
        echo '<div class="homepage-welcome-block-content-left">';
        echo do_shortcode(wp_kses_post($himmelen_theme_options['blog_homepage_welcome_block_left_content']));
        echo '</div>';
        echo '</div>';
    }

    if(isset($himmelen_theme_options['blog_enable_homepage_welcome_block_center']) && $himmelen_theme_options['blog_enable_homepage_welcome_block_center']) {
        echo '<div class="'.esc_attr($col_center_class).'">';
        echo '<div class="homepage-welcome-block-content-center">';
        echo do_shortcode(wp_kses_post($himmelen_theme_options['blog_homepage_welcome_block_center_content']));
        echo '</div>';
        echo '</div>';
    }

    if(isset($himmelen_theme_options['blog_enable_homepage_welcome_block_right']) && $himmelen_theme_options['blog_enable_homepage_welcome_block_right']) {
        echo '<div class="'.esc_attr($col_right_class).'">';
        echo '<div class="homepage-welcome-block-content-right">';
        echo do_shortcode(wp_kses_post($himmelen_theme_options['blog_homepage_welcome_block_right_content']));
        echo '</div>';
        echo '</div>';
    }

    echo '</div>';
    echo '</div>';

    echo '</div>';

}

function himmelen_welcome_block_2_show() {

    global $himmelen_theme_options;

    echo '<div class="homepage-welcome-block-2">';
    
    echo '<div class="homepage-welcome-block-2-content">';
    echo do_shortcode(wp_kses_post($himmelen_theme_options['blog_homepage_welcome_block_2_content']));
    echo '</div>';

    echo '</div>';

}

/* Blog post excerpt & read more */
function himmelen_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'himmelen_excerpt_more');


function himmelen_modify_read_more_link() {
    return '<a class="more-link btn alt" href="' . get_permalink() . '">Continue reading</a>';
}
add_filter( 'the_content_more_link', 'himmelen_modify_read_more_link' );

/* Coming soon mode */
if(isset($himmelen_theme_options['enable_comingsoon']) && $himmelen_theme_options['enable_comingsoon']) {

    function himmelen_maintenance_mode_check_msg() {   
        if(get_current_screen()->parent_base !== 'ipanel_HIMMELEN_PANEL') {
             $msg = '<p><span style="color: red">The Maintenance/Coming soon Mode is active, your website is not visible for regular visitors.</span> Please don\'t forget to <a href="'.get_admin_url( '','admin.php?page=ipanel_HIMMELEN_PANEL').'">deactivate it</a> when you will be ready to go live.</p>';
            echo '<div class="updated fade">'.wp_kses_post($msg).'</div>';
        }
    }

    if (is_admin()) {
        add_action('admin_notices', 'himmelen_maintenance_mode_check_msg');
    }

    function himmelen_toolbar_link_comingsoon( $wp_admin_bar ) {
        $args = array(
            'id'    => 'coming_soon_mode_active',
            'title' => 'Coming soon mode active',
            'href'  => get_admin_url( '','admin.php?page=ipanel_HIMMELEN_PANEL'),
            'meta'  => array( 'class' => 'wpadminbar-coming-soon-active' )
        );
        $wp_admin_bar->add_node( $args );
    }
    add_action( 'admin_bar_menu', 'himmelen_toolbar_link_comingsoon', 999 );

    if ( !is_admin_bar_showing() && !is_admin() ) {
        if (!current_user_can('edit_posts') && !in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ))) {

            function himmelen_maintenance_mode_show() {

                $current_url_http = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $current_url_https = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                $pages = get_pages(array(
                    'meta_key' => '_wp_page_template',
                    'meta_value' => 'coming-soon-page.php'
                ));

                if(($current_url_http !== get_permalink( $pages[0]->ID ))&&(($current_url_https !== get_permalink( $pages[0]->ID )))) {

                        wp_redirect( get_permalink( $pages[0]->ID ) );
                    exit;
                    
                }
                
            }

            add_action('init', 'himmelen_maintenance_mode_show');

        }
    }
}

// Custom User social profiles
function himmelen_add_to_author_profile( $contactmethods ) {

    $social_array = array('facebook' => 'Facebook', 'twitter' => 'Twitter', 'vk' => 'Vkontakte', 'google-plus' => 'Google Plus', 'behance' => 'Behance', 'linkedin' => 'LinkedIn', 'pinterest' => 'Pinterest', 'deviantart' => 'DeviantArt', 'dribbble' => 'Dribbble',  'flickr' => 'Flickr', 'instagram' => 'Instagram', 'skype' => 'Skype', 'tumblr' => 'Tumblr', 'twitch' => 'Twitch', 'vimeo-square' => 'Vimeo', 'youtube' => 'Youtube');
    
    foreach ($social_array as $social_key => $social_value) {
        # code...
        $contactmethods[$social_key.'_profile'] = $social_value.' Profile URL';
    }
    
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'himmelen_add_to_author_profile', 10, 1);

// Count post views
function himmelen_getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return 0;
    }
    return $count;
}

function himmelen_setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); 

/**
 * Function for outputting a cmb2 file_list
 *
 * @param  string  $file_list_meta_key The field meta key. ('wiki_test_file_list')
 * @param  string  $img_size           Size of image to show
 */
function himmelen_cmb2_get_images_src( $post_id, $file_list_meta_key, $img_size = 'medium' ) {

    // Get the list of files
    $files = get_post_meta( $post_id, $file_list_meta_key, 1 );

    $attachments_image_urls_array = Array();

    foreach ( (array) $files as $attachment_id => $attachment_url ) {
        
        $current_attach = wp_get_attachment_image_src( $attachment_id, $img_size );

        $attachments_image_urls_array[] = $current_attach[0];
     
    }

    if($attachments_image_urls_array[0] == '') {
        $attachments_image_urls_array = array();
    }
     
    return $attachments_image_urls_array;
    
}



/**
 * @param  string  $filename
 * @return string
 */
function asset_path($filename) {
    $manifest_path = get_stylesheet_directory_uri() .'/rev-manifest.json';
    if ( file_exists($manifest_path ) ) {
        $manifest = json_decode( file_get_contents( $manifest_path ), TRUE );
    } else {
        $manifest = [];
    }
    if ( array_key_exists( $filename, $manifest ) ) {
        return $manifest[$filename];
    }
    return $filename;
}

if ( ! function_exists( 'compareclub_assets' ) ) {
  /**
   * Load theme's JavaScript and CSS sources.
   */
  function compareclub_assets() {

    if( is_singular('post') ) {
      wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri() . '/'. asset_path('css/theme.min.css'), array(), null);

      wp_enqueue_script( 'jquery');
      wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false, true);
      wp_enqueue_script( 'chart-js', get_template_directory_uri() . '/js/chart.js', array(), false, false);

      wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/' . asset_path('js/theme.js'), array(), null, true );
      if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
      }
    }
    
  }
} // endif function_exists( 'compareclub_assets' ).

add_action( 'wp_enqueue_scripts', 'compareclub_assets' );


/**
 * Guide Item
 * Renders Guide Items with grid layout
 * @return html
 */
if( !function_exists('cc_guides') ):
  function cc_guides() { ?>
    <div class="row align-items-center mt-md-5 pt-5">
      <div class="col-lg-4 col-md-12 d-none d-lg-block">
        <div class="cc-guide-item">
          <div class="thumb">
            <div class="caption">
              <div class="cat">
                <a href="#">HEALTH &amp; LIFESTYLE</a>
              </div>
              <h4><a href="#">Buying Your First Home? Read This First!</a></h4>
              <div class="date">August 2018</div>
            </div>
            <img src="<?php echo get_template_directory_uri(); ?>/img/cat-services-3/guide-1.jpg" alt="">
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <div class="row">
          <div class="col-md-6 d-block d-lg-none">
            <div class="cc-guide-item">
              <div class="thumb">
                <div class="caption">
                  <div class="cat">
                    <a href="#">HEALTH &amp; LIFESTYLE</a>
                  </div>
                  <h4><a href="#">Buying Your First Home? Read This First!</a></h4>
                  <div class="date">August 2018</div>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/img/cat-services-3/guide-1.jpg" alt="">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="cc-guide-item">
              <div class="thumb">
                <div class="caption">
                  <div class="cat">
                    <a href="#">HEALTH &amp; LIFESTYLE</a>
                  </div>
                  <h4><a href="#">Buying Your First Home? Read This First!</a></h4>
                  <div class="date">August 2018</div>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/img/cat-services-3/guide-2.jpg" alt="">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="cc-guide-item">
              <div class="thumb">
                <div class="caption">
                  <div class="cat">
                    <a href="#">HEALTH &amp; LIFESTYLE</a>
                  </div>
                  <h4><a href="#">Buying Your First Home? Read This First!</a></h4>
                  <div class="date">August 2018</div>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/img/cat-services-3/guide-3.jpg" alt="">
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-12 f-hor">
            <div class="cc-guide-item">
              <div class="thumb">
                <div class="caption f-width">
                  <div class="cat">
                    <a href="#">HEALTH &amp; LIFESTYLE</a>
                  </div>
                  <h4><a href="#">Buying Your First Home? Read This First!</a></h4>
                  <div class="date">August 2018</div>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/img/cat-services-3/guide-4.jpg" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php }
endif;


/**
 * CTA JOIN US
 * Render CTA "Join us now it's free"
 * @return string
 */
if( !function_exists('cc_join_us') ):
  function cc_join_us() { ?>
    <div class="card rds-0 py-4 px-3">
      <div class="row justify-content-center align-items-center">
        <div class="col-md-3 col-lg-4 text-center text-md-right">
          <img src="<?php echo get_template_directory_uri(); ?>/img/readytosave.png" alt="">
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-6 col-lg-5 text-center text-md-left mt-4 mt-md-0">
          <div class="text-semilarge mb-3">Ready to save?</div>
          <a href="/user/sign-up" class="btn btn-primary">Join the Club</a>
        </div>
      </div>
    </div>
<?php
  }
endif;

/**
 * Include a Post Template rule type
 */
function acf_post_template_rule_type( $rule_types ) {
  if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
      return $rule_types;
  }
  $rule_types['Post']['post_template'] =  __("Post Template",'acf');
  return $rule_types;
}
add_filter('acf/location/rule_types', 'acf_post_template_rule_type', 10, 1);
/**
 * Supply values for the Post Template rule type
 */
function acf_post_template_rule_values ( $choices ) {
  if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
      return $choices;
  }
  $args = array('public' => true, 'capability_type' => 'post');
  $post_types = get_post_types( $args );
  $post_templates = array( 'none' => 'None' );
  foreach ($post_types as $key => $post_type) {
    $post_templates = array_merge($post_templates, get_page_templates(null, $post_type) );
  }
  foreach( $post_templates as $k => $v ) {
    $choices[ $v ] = $k;
  }
  return $choices;
}
add_filter( 'acf/location/rule_values/post_template', 'acf_post_template_rule_values', 10, 1 );
/**
 * Match the rule type and edit screen
 */
add_filter('acf/location/rule_match/post_template', 'acf_location_rules_match_post_template', 10, 3);
function acf_location_rules_match_post_template( $match, $rule, $options ) {
  if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
      return $match;
  }
  // copied from acf_location::rule_match_page_template (advanced-custom-fields-pro/core/location.php)
  // bail early if not a post
  if( !$options['post_id'] ) return false;
  // vars
  $page_template = $options['page_template'];
  // get page template
  if( !$page_template ) {
    $page_template = get_post_meta( $options['post_id'], '_wp_page_template', true );
  }
  // get page template again
  if( !$page_template ) {
    $post_type = $options['post_type'];
    if( !$post_type ) {
      $post_type = get_post_type( $options['post_id'] );
    }
    if( $post_type === 'page' ) {
      $page_template = "default";
    }
  }
  // compare
  if( $rule['operator'] == "==" ) {
    $match = ( $page_template === $rule['value'] );
  } elseif( $rule['operator'] == "!=" ) {
    $match = ( $page_template !== $rule['value'] );
  }
  // return
  return $match;
}