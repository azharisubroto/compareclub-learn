<?php
/**
 * @package Himmelen
 */
global $himmelen_theme_options, $post_loop_id, $wp_embed, $span_class;

$js_script = '';

$post_classes = get_post_class();

$current_post_format = $post_classes[4];

$post_formats_media = array('format-audio', 'format-video', 'format-gallery');
$post_formats_hidereadmore = array('format-quote', 'format-link', 'format-status');

$post_socialshare_disable = get_post_meta( get_the_ID(), '_post_socialshare_disable_value', true );

// Blog layout
if(isset($himmelen_theme_options['blog_layout'])) {
	$blog_layout = $himmelen_theme_options['blog_layout'];
} else {
	$blog_layout = 'layout_default';
}

if($blog_layout == 'layout_vertical_design') {
	$blog_enable_vertical_post_design = true;
} else {
	$blog_enable_vertical_post_design = false;
}

if($blog_layout == 'layout_2column_design') {
	$blog_enable_2_column_design = true;
} else {
	$blog_enable_2_column_design = false;
}

if($blog_layout == 'layout_masonry') {
	$blog_enable_masonry_design = true;
} else {
	$blog_enable_masonry_design = false;
}

if(!isset($post_loop_id)) {
	$post_loop_id = 1;
}

if(($post_loop_id % 3 == 0)&&has_post_thumbnail( get_the_ID() )&&$blog_enable_vertical_post_design&&!in_array($current_post_format, $post_formats_media)) {
	$current_post_vertical = true;
} else {
	$current_post_vertical = false;
}

if($blog_layout == 'layout_list') {
	$current_post_list = true;
	$post_formats_media = array('format-audio', 'format-video');
} else {
	$current_post_list = false;
}

if(is_sticky(get_the_ID())) {
	$current_post_sticky = true;
	$sticky_post_class = 'sticky';
} else {
	$current_post_sticky = false;
	$sticky_post_class = '';
}

$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'blog-thumb');

if(has_post_thumbnail( get_the_ID() )) {
    $image_bg ='background-image: url('.$image[0].');';
}
else {
    $image_bg = '';
}

// Post Format options
// $current_post_format:
// format-gallery
// format-video
// format-audio

$post_embed_video = get_post_meta( get_the_ID(), '_himmelen_video_embed', true );

if($post_embed_video !== '') {
	$post_embed_video_output = $wp_embed->run_shortcode('[embed width="auto" height="660"]'.$post_embed_video.'[/embed]');

} else {
	$post_embed_video_output = '';
}

$post_embed_audio = get_post_meta( get_the_ID(), '_himmelen_audio_embed', true );

if($post_embed_audio !== '') {
	$post_embed_audio_output = $wp_embed->run_shortcode('[embed]'.$post_embed_audio.'[/embed]');

} else {
	$post_embed_audio_output = '';
}

$gallery_images_data = himmelen_cmb2_get_images_src( get_the_ID(), '_himmelen_gallery_file_list', 'blog-thumb' );

if($gallery_images_data !== '') {

	$post_gallery_id = 'blog-post-gallery-'.get_the_ID();
	$post_embed_gallery_output = '<div class="blog-post-gallery-wrapper" id="'.$post_gallery_id.'" style="display: none;">';

	foreach ($gallery_images_data as $gallery_image) {
		$post_embed_gallery_output .= '<div class="blog-post-gallery-image"><a href="'.get_the_permalink().'"><img src="'.$gallery_image.'" alt="'.get_the_title().'"/></a></div>';
	}

	$post_embed_gallery_output .= '</div>';

	$js_script = '<script>(function($){
            $(document).ready(function() {

                $("#'.$post_gallery_id.'").owlCarousel({
                    items: 1,
                    itemsDesktop:   [1199,1],
                    itemsDesktopSmall: [979,1],
                    itemsTablet: [768,1],
                    itemsMobile : [479,1],
                    autoPlay: true,
                    autoHeight: true,
                    navigation: true,
                    navigationText : false,
                    pagination: false,
                    afterInit : function(elem){
                        $(this).css("display", "block");
                    }
                });
            
            });})(jQuery);</script>';
	
} else {
	$post_embed_gallery_output = '';
}

?>
<?php if(!has_post_thumbnail( get_the_ID() )) { $sticky_post_class .= ' sticky-post-without-image'; } else { $sticky_post_class .= ''; } ?>
<div class="content-block blog-post clearfix<?php if($current_post_vertical) { echo ' blog-post-vertical';} if($current_post_list) { echo ' blog-post-list-layout';} if($blog_enable_2_column_design) { echo ' blog-post-2-column-layout';}?>">
	<article id="post-<?php the_ID(); ?>" <?php post_class($sticky_post_class); ?>>

		<div class="post-content-wrapper"<?php if(($current_post_sticky)&&($blog_enable_masonry_design)&&($image_bg !== '')&&(!in_array($current_post_format, $post_formats_media))) { echo ' data-style="'.esc_attr($image_bg).'"'; } ?>>
			<?php if(($current_post_sticky)&&($blog_enable_masonry_design)&&($image_bg !== '')): ?>
				<div class="sticky-post-badge<?php if(!has_post_thumbnail( get_the_ID() )) { echo ' sticky-post-without-image'; } ?>"><?php _e('Featured', 'himmelen'); ?></div>
			<?php endif; ?>
			<?php 
			// Vertical post thumb
			if($current_post_vertical):
			?>
			<div class="blog-post-thumb">
				<a href="<?php the_permalink(); ?>" rel="bookmark">
				<?php the_post_thumbnail('blog-thumb-vertical'); ?>
				</a>
			</div>
			<?php endif;?>
			<?php 
			// List post thumb
			if(($current_post_list)&&!$current_post_vertical&&!$blog_enable_masonry_design):
			?>
				<?php
	          	if(($image_bg !== '')&&(!in_array($current_post_format, $post_formats_media))):
				?>
				<a class="blog-post-thumb" href="<?php the_permalink(); ?>" rel="bookmark" data-style="<?php echo esc_attr($image_bg); ?>"></a>
				<?php endif;?>
				<?php 
					if(in_array($current_post_format, $post_formats_media)) {
						echo '<div class="blog-post-thumb">';

					// Post media
					if($current_post_format == 'format-video') {
						echo '<div class="blog-post-media blog-post-media-video">';
						echo ($post_embed_video_output);// escaping does not needed here, wordpress OEMBED function used for this var
						echo '</div>';
					}
					elseif($current_post_format == 'format-audio') {
						echo '<div class="blog-post-media blog-post-media-audio">';
						echo ($post_embed_audio_output);// escaping does not needed here, wordpress OEMBED function used for this var
						echo '</div>';
					}
					elseif($current_post_format == 'format-gallery') {
						echo '<div class="blog-post-media blog-post-media-gallery clearfix">';
						echo wp_kses_post($post_embed_gallery_output);
						echo '</div>';

						echo ($js_script); // this var is safe and can't be escaped via wordpress functions, because contain js+jquery+html, escaping will break the code here
					}
						echo '</div>';
					}
				
				?>
			<?php endif;?>
			
			<?php 
			// Masonry thumbnail
			if($blog_enable_masonry_design) {

				if ( has_post_thumbnail() && (!$current_post_sticky)&&!in_array($current_post_format, $post_formats_media) ):
				?>

				<div class="blog-post-thumb">
					<a href="<?php the_permalink(); ?>" rel="bookmark">
					<?php the_post_thumbnail('blog-thumb'); ?>
					</a>
				</div>
				<?php
				endif;

				// Masonry media posts
				if(in_array($current_post_format, $post_formats_media)) {
					echo '<div class="blog-post-thumb">';

				// Post media
				if($current_post_format == 'format-video') {
					echo '<div class="blog-post-media blog-post-media-video">';
					echo ($post_embed_video_output);// escaping does not needed here, wordpress OEMBED function used for this var
					echo '</div>';
				}
				elseif($current_post_format == 'format-audio') {
					echo '<div class="blog-post-media blog-post-media-audio">';
					echo ($post_embed_audio_output);// escaping does not needed here, wordpress OEMBED function used for this var
					echo '</div>';
				}
				elseif($current_post_format == 'format-gallery') {
					echo '<div class="blog-post-media blog-post-media-gallery clearfix">';
					echo wp_kses_post($post_embed_gallery_output);
					echo '</div>';

					echo ($js_script);// this var is safe and can't be escaped via wordpress functions, because contain js+jquery+html, escaping will break the code here
				}
					echo '</div>';
				}
			}
			?>
			<?php 
			// Regular post thumb
			if((!$current_post_vertical)&&(!$current_post_list)&&(!$blog_enable_masonry_design)) {
				
				// Post media
				if($current_post_format == 'format-video') {
					echo '<div class="blog-post-thumb">';
					echo '<div class="blog-post-media blog-post-media-video">';
					echo ($post_embed_video_output);// escaping does not needed here, wordpress OEMBED function used for this var
					echo '</div>';
					echo '</div>';
				}
				elseif($current_post_format == 'format-audio') {
					echo '<div class="blog-post-thumb">';
					echo '<div class="blog-post-media blog-post-media-audio">';
					echo ($post_embed_audio_output);// escaping does not needed here, wordpress OEMBED function used for this var
					echo '</div>';
					echo '</div>';
				}
				elseif($current_post_format == 'format-gallery') {
					echo '<div class="blog-post-thumb">';
					echo '<div class="blog-post-media blog-post-media-gallery clearfix">';
					echo wp_kses_post($post_embed_gallery_output);
					echo '</div>';
					echo '</div>';

					echo ($js_script);// this var is safe and can't be escaped via wordpress functions, because contain js+jquery+html, escaping will break the code here
				} else {
					// Post thumbnail
					if ( has_post_thumbnail() ):

						$blog_thumb_size = 'blog-thumb';

						if($blog_enable_2_column_design && ($post_loop_id > 1) && $span_class == 'col-md-9') {
							$blog_thumb_size = 'blog-thumb-2column-sidebar';
						}
						
					?>
						<div class="blog-post-thumb">
						<a href="<?php the_permalink(); ?>" rel="bookmark">
						<?php the_post_thumbnail($blog_thumb_size); ?>
						</a>
						</div>
					
					<?php
					endif;
				}

				
			}
			
			?>
			<div class="post-content">

		
				<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list(  ', '  );
				if ( $categories_list ) :
				?>
			
				<div class="post-categories"><?php printf( __( '%1$s', 'himmelen' ), $categories_list ); ?></div>
				
				<?php endif; // End if categories ?>
		

				<h2 class="entry-title post-header-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?><?php if($current_post_sticky&&!$blog_enable_masonry_design) { echo '<sup>'.__('Featured', 'himmelen').'</sup>'; } ?></a></h2>
				<div class="post-info-date"><?php the_time(get_option( 'date_format' ));  ?></div>
			
				
				<div class="entry-content">
					<?php 
					
					// Post content
					if($blog_enable_masonry_design) {
						the_excerpt();
						?>
						<?php if(!in_array($current_post_format, $post_formats_hidereadmore)):?>
						<a href="<?php the_permalink(); ?>" class="more-link btn alt"><?php _e('Continue reading', 'himmelen'); ?></a>
						<?php endif;?>
						<?php
			
					} else {
				
						if(isset($himmelen_theme_options['blog_post_loop_type']) && $himmelen_theme_options['blog_post_loop_type']=='excerpt') {
							the_excerpt();

							if(!in_array($current_post_format, $post_formats_hidereadmore)) {
								echo '<a class="more-link btn alt" href="'.get_permalink().'">'.__('Continue reading', 'himmelen').'</a>';
							}

						} else {
							
							the_content('');
							
							?>
							<a href="<?php the_permalink(); ?>" class="more-link btn alt"><?php echo __('Continue reading', 'himmelen'); ?></a>
							<?php
						}

						wp_link_pages( array(
							'before' => '<div class="page-links">' . __( 'Pages:', 'himmelen' ),
							'after'  => '</div>',
						) );
					}
					
					?>
				</div><!-- .entry-content -->

				
			</div>
			<div class="clear"></div>
			<?php if((!$current_post_list)&&(!$blog_enable_masonry_design)):?>
			<div class="post-info clearfix">
				<?php if(isset($himmelen_theme_options['blog_post_show_author'])&&($himmelen_theme_options['blog_post_show_author'])): ?>
				<div class="post-author"><?php esc_html_e('by','himmelen'); ?> <?php the_author();?></div>
				<?php endif; ?>
				
				<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>

				<div class="comments-count"><?php comments_popup_link( __( 'Leave a comment', 'himmelen' ), __( '1 Comment', 'himmelen' ), __( '% Comments', 'himmelen' ) ); ?></div>
				<?php endif; ?>

				<?php if(!$blog_enable_masonry_design):?>
					<?php if(!isset($post_socialshare_disable) || !$post_socialshare_disable): ?>
						<div class="share-post">
						<?php get_template_part( 'share-post' ); ?>
						</div>
					<?php endif; ?>
				<?php endif; ?>
				
			</div>
			<?php endif; ?>

		</div>

	</article>
	<?php if(isset($himmelen_theme_options['blog_list_show_related'])&&($himmelen_theme_options['blog_list_show_related'])&&!$blog_enable_masonry_design&&!$blog_enable_2_column_design): ?>
		<?php get_template_part( 'related-posts-loop' ); ?>
	<?php endif; ?>
</div>

