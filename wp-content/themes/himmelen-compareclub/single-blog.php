<?php
/**
 * Template Name: Blog
 * Template Post Type: post
 * @package compareclub
 */

get_header();

// Count view
himmelen_setPostViews(get_the_ID());

?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'content', 'single' ); ?>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>