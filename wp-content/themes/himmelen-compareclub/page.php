<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Himmelen
 */

get_header('compareclub');

?>

<?php if( have_posts() ): ?>
		<?php while( have_posts() ): the_post(); ?>
			<!-- HERO -->
			<section class="py-5">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-11 text-lightgray text-semimedium">
							<h3 class="guide-headline">
								<?php the_title(); ?>
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php if( has_post_thumbnail() ): ?>	
								<figure class="mt-4 mb-5">
									<?php the_post_thumbnail('full'); ?>
								</figure>								
							<?php endif; ?>

							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</section>
			<hr>
			<main class="main-parent position-relative">
				<?php $array = get_field('guide_content'); ?>
				<?php if( !empty($array) ): ?>
					<?php 
						$s=0;
						foreach( $array as $section ): 
						$background = $section['background'];
						$tab = $section['tab'];
					?>
						<section class="py-0 first-section position-static" id="step-0" <?php if( $background ) echo 'style="background:'.$background.'"'; ?>>
							<div class="container position-static">
								<div class="row position-static">
									<div class="col-lg-4 d-none d-md-block position-static">
										<?php 
											$i=0;
											echo '<div id="bla-'.$s.'" class="pt-4 nav flex-column cc-tab-buttons ctb-v2 brd-0 smoothscroll mr-5 stick-this-blog">';
											foreach( $tab as $tabcontent ) {
												if( $i == $s ) {
													$isactive = 'active';
												} else {
													$isactive = '';
												}
												echo '<a class="nav-link '.$isactive.'" href="#step-'.$i.'">';
												echo $tabcontent['title'];
												echo '</a>';
												$i++;
											}
											echo '</div>';
										 ?>
									</div>
									<div class="col-lg-8">
										<div class="pl-3 my-0 pt-1">
											<?php 
												$i=0;
												foreach( $tab as $tabcontent ) {
													if( $i > 0 ) break;
													echo '<div id="'.str_replace(' ', '', $tabcontent['title']).'">';
													echo $tabcontent['content'];
													echo '</div>';
													$i++;
												}
											 ?>
										</div>
									</div>
								</div>
							</div>
						</section>
						<?php 
							$s=0;
							foreach( $tab as $tabcontent ) {
								if( $s > 0 ){
									echo '<section class="py-0" id="step-'.$s.'">';
									echo '<div class="container">';
									echo '<div class="row">';
						?>
											<div class="col-lg-4">
												&nbsp;
											</div>
						<?php
									echo '<div class="col-lg-8">';
									echo $tabcontent['content'];
									echo '</div>';
									echo '</div>';
									echo '</div>';
									echo '</section>';
								}
								$s++;
							}
						 ?>
					<?php $s++; endforeach; ?>
				<?php endif; ?>
				<div class="clearfix"></div>
			</main>
		<?php 
			$postcat = get_the_category( get_the_ID() ); 
		endwhile; ?>
	<?php endif; ?>

	<hr class="my-0">
	<section class="pb-0">
		<div class="container">
			<div class="row lh-1 mt-5">
				<div class="col-12">
					<h2 class="text-52 text-xs-30">You may also like…</h2>
				</div>
			</div>

			<?php 
			cc_guides($postcat); ?>
		</div>
	</section>
	<section class="div container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<?php cc_join_us(); ?>
			</div>
		</div>
	</section>

<?php get_footer('compareclub'); ?>
