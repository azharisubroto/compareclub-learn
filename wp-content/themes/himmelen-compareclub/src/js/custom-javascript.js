var $ = jQuery;

jQuery(window).load(function(){
	// Elements to inject
	var mySVGsToInject = document.querySelectorAll('img.svg-inject');
	  // Do the injection
	SVGInjector(mySVGsToInject);
});

$(document).ready(function($){

	// MEGA MENU
	$('.cc-m-menu').click(function(){
		$(this).toggleClass('default letclose');
		$('.cc-nav-menu').toggleClass('d-none d-block');
	});

	// Mega menu tab centering behaviour
	if( !isMobile.any ) {
		$('ul .has-mega-menu:not(.first)').each(function(){
			var $ini = $(this),
				$offset = $ini.offset(),
				$left = $offset.left,
				$left = parseInt($left + 25),
				$navwidth = $(this).find('.nav-pills').outerWidth();
			$ini.find('.nav-pills').css('left', $left);
		});
		$('.cc-mm-bar .nav-link').hover(function(){
		  $(this).trigger('click');
		});
	}

	if( isMobile.any ) {
		$('.has-mega-menu a').on('click', function(e){
			e.preventDefault();
			$(this).next('.cc-mega-menu').toggle();
			$(this).parent('.has-mega-menu').siblings('.has-mega-menu').find('.cc-mega-menu').hide();
		});
	}

	// Testimonial slide for mobile only
	if( isMobile.apple.phone || isMobile.android.phone || isMobile.seven_inch ) {
		if( $('.cc-testi-wrap').length ) {
			var elem = document.querySelector('.cc-testi-wrap');
			var flkty = new Flickity( elem, {
				// options
				cellAlign: 'left',
				contain: false,
				prevNextButtons: false
			});
		}		
	}

	// Table Slide for mobile only
	if( isMobile.apple.phone || isMobile.android.phone || isMobile.seven_inch ) {
		if( $('.cc-m-bl').length ) {
			var elem = document.querySelector('.cc-m-bl');
			var flkty = new Flickity( elem, {
				// options
				cellAlign: 'left',
				contain: false,
				prevNextButtons: false
			});
		}
		if( $('.cc-table-slide').length ) {
			var elem = document.querySelector('.cc-table-slide');
			var flkty = new Flickity( elem, {
				// options
				cellAlign: 'left',
				contain: false,
				prevNextButtons: false
			});
		}
	}

	// COMCARD
	if( isMobile.apple.phone || isMobile.android.phone || isMobile.seven_inch ) {
		if( $('.comcards').length ) {
			var comcards = document.querySelector('.comcards');
			var comcardsflick = new Flickity( comcards, {
				cellAlign: 'left',
				contain: true,
				freeScroll: true,
				prevNextButtons: false
			});
		}
	}

	// Testimonial Slide
	if( $('.cc-testi-slide').length ) {
		var elem = document.querySelector('.cc-testi-slide');
		var flkty = new Flickity( elem, {
			// options
			cellAlign: 'left',
			contain: false,
			prevNextButtons: true,
			on: {
			    ready: function() {
			      	var $parentwidth = $('.cc-testi-slide').outerWidth() / 2;
			      	var $pagingwidth = $('.flickity-page-dots').outerWidth();
			      	var $distance = $parentwidth - $pagingwidth;
			      	$('.cc-testi-slide .flickity-button.previous').css('left', $distance);
			      	$('.cc-testi-slide .flickity-button.next').css('right', $distance);
			      	$( window ).resize(function() {
			      		setTimeout(function() {
			      			var $parentwidth = $('.cc-testi-slide').outerWidth() / 2;
			      			var $pagingwidth = $('.flickity-page-dots').outerWidth();
			      			var $distance = $parentwidth - $pagingwidth;
			      			$('.cc-testi-slide .flickity-button.previous').css('left', $distance);
			      			$('.cc-testi-slide .flickity-button.next').css('right', $distance);
			      			//console.log($distance);
			      		}, 300);
			      	});
			    }
			}
		});
	}

	// Toggle reead less read more for homepage
	$('.extra-toggle').click(function(e){
		e.preventDefault();
		$(this).text(function(i, text){
		    return text === "Read more" ? "Read less" : "Read more";
		});
		$(this).next('.cc-extra').toggleClass('d-none d-block');
	});

	// Section tab scroll
	if( isMobile.tablet || isMobile.phone ) { 
		$('.smoothscroll > a').on('click', function(e){
			e.preventDefault();
			var targed = $(this).attr('href');

			$(this).addClass('active').siblings('a').removeClass('active');
			$('.scrollspy-area '+targed).show().siblings('.scrollspy-area > div').hide();
		});
	}

	// If not mobile, make it a tab
	if( ! isMobile.any ) {

		$('.smoothscroll > a').on('click', function(e){
			e.preventDefault();
			var targed = $(this).attr('href');
			var topset = $(targed).offset().top;

			//$(this).addClass('active').siblings('a').removeClass('active');
			$('html, body').animate({
		        scrollTop: topset
		    }, 300);

		    if( $('.pageindex').length ) {
		    	$(this).addClass('active').siblings('a').removeClass('active');
		    }
		});

		// Sticky Sidebar for Guide Page
		$('.stick-this').stick_in_parent();

		// Sticky Sidebar for Guide Blog
		$('.stick-this-blog').stick_in_parent({
			parent: '.main-parent'
		});

		$('.stick-this-blog').on('sticky_kit:bottom', function(e) {
            $(this).parent().css('absolute', 'fixed');
            $(this).parent().css('bottom', '0');
        }).on('sticky_kit:unbottom', function(e) {
            $(this).parent().css('position', 'relative');
        });

		$('.scrollspy-area div').each(function(e) {
			var ini = $(this);
			var id = ini.attr('id');

			if( !$('.pageindex').length ) {
				ini.waypoint(function(direction) {
					if (direction === 'down') {
						$('.smoothscroll.nav > a[href="#'+id+'"]').addClass('active').siblings('a').removeClass('active');
					}
				}, {
				  	offset: 50
				});

				ini.waypoint(function(direction) {
					if (direction === 'up') {
						$('.smoothscroll.nav > a[href="#'+id+'"]').addClass('active').siblings('a').removeClass('active');
					}
				}, {
				  offset: 10
				});
			}			
		});

		$('.main-parent section').each(function(e) {
			var ini = $(this);
			var id = ini.attr('id');

			if( !$('.pageindex').length ) {
				ini.waypoint(function(direction) {
					if (direction === 'down') {
						$('.smoothscroll.nav > a[href="#'+id+'"]').addClass('active').siblings('a').removeClass('active');
					}
				}, {
				  	offset: 50
				});

				ini.waypoint(function(direction) {
					if (direction === 'up') {
						$('.smoothscroll.nav > a[href="#'+id+'"]').addClass('active').siblings('a').removeClass('active');
					}
				}, {
				  offset: 0
				});
			}			
		});
	} 

	// Guide Slide Calculator
	if( $('#anualincome').length ) {
		var $income = document.getElementById('anualincome');
		var $min = $('#anualincome').data('min'),
			$max = $('#anualincome').data('max')
		noUiSlider.create($income, {
			start: 500,
			connect: [true, false],
			range: {
				'min': 0,
				'10%': 100,
				'20%': 200,
				'30%': 300,
				'40%': 400,
				'50%': 500,
				'max': 1000
			}
		});

		var nonLinearSliderValueElement = document.getElementById('annualincome-update');

		// Show the value for the *last* moved handle.
		$income.noUiSlider.on('update', function (values, handle) {
		    nonLinearSliderValueElement.innerHTML = values[handle];
		});
	}
	
	if( $('#yourage').length ) {
		var $yourage = document.getElementById('yourage');
		var $min = $('#yourage').data('min'),
			$max = $('#yourage').data('max')
		noUiSlider.create($yourage, {
			start: 37,
			connect: [true, false],
			step: 1,
			range: {
				'min': 18,
				'max': 100
			},
			ariaFormat: wNumb({
		        decimals: 0
		    }),
		    format: wNumb({
		        decimals: 0,
		        thousand: '.',
		    })
		});

		var yourageupdate = document.getElementById('yourage-update');

		// Show the value for the *last* moved handle.
		$yourage.noUiSlider.on('update', function (values, handle) {
		    yourageupdate.innerHTML = values[handle];
		});
	}

	/**
	 * GROCERIES
	 */
	// Fancy Select box
	$('select:not(.multiple-select)').niceSelect();
	//$('.js-example-basic-multiple').select2();

	// Sticky Search bar
	if( $('#scroll-indicator').length ) {
		var distance = $('#scroll-indicator').offset().top,
		    $window = $(window);

		$window.scroll(function() {
		    if ( $window.scrollTop() >= distance ) {
		        $('.groceries-bar').addClass('appear');
		    }
		    if ( $window.scrollTop() <= distance ) {
		        $('.groceries-bar').removeClass('appear');
		    }
		});
	}

	// Groceries Multiselect
	$('.multiple-select').each(function(){
		$(this).multiselect();
	});

	// Move all modals to before the end of </body>
	$('.modal').appendTo('body');

	// Slideshow on Modalbox
	$('.groc-modal').on('shown.bs.modal', function(){
		$(this).find('.groc-rel-slide').flickity({
			cellSelector: '.cell-item',
			pageDots: false,
			cellAlign: 'left',
			wrapAround: true,
			contain: true,
			autoPlay: false
		});
	});

	$('.groc-modal').on('hidden.bs.modal', function(){
		$(this).find('.groc-rel-slide').flickity('destroy');
	});

	if( $('.groc-rel-slide').length ) {
		$('.groc-rel-slide').flickity({
			cellSelector: '.cell-item',
			pageDots: false,
			cellAlign: 'left',
			wrapAround: true,
			contain: true,
			autoPlay: false
		});
	}

	//$('select').each(function(){
		/*$('select').not('.postcode, .nochosen').chosen({
		    no_results_text: "Oops, nothing found!",
		    width: "100%"
	  	});*/
	//});	

	// RANGE SLIDER
	if( $('#pricerange').length ) {
		var slider = document.getElementById('pricerange');
		var $min = $('#pricerange').data('min'),
			$max = $('#pricerange').data('max')
		noUiSlider.create(slider, {
			start: [ $min, $max ],
			connect: true,
			range: {
				'min': 0,
				'10%': 10,
				'20%': 20,
				'30%': 30,
				'40%': 40,
				'50%': 50,
				'max': 100
			}
		});

		// Declare the id of price range
		var snapValues = [
			document.getElementById('min'),
			document.getElementById('max')
		];

		slider.noUiSlider.on('update', function( values, handle ) {
			snapValues[handle].value = values[handle];
		});

	}

	$('.filter-rst').click(function(e){
		$('select').niceSelect('update');
	});

	$('#groc-load-more').on('click', function(e) {
		e.preventDefault();
		$('.cc-groc-products .col-md-3').clone().appendTo( ".cc-groc-products" );
	});

	// PETROL PAGE: GEO LOCATION
	if( $('#container_chart').length ) {
		var x = document.getElementById("location");

		function getLocation() {
		    if (navigator.geolocation) {
		        navigator.geolocation.getCurrentPosition(showPosition);
		    } else { 
		        x.value = "Geolocation is not supported by this browser.";
		    }
		}

		function showPosition(position) {
		    x.value = position.coords.latitude + ", " + position.coords.longitude;
		}


		$('.location-btn').on('click', function(){
			getLocation();
		});

		// PETROL PAGE: CHART
		var categories = [];
		var series = [];

		for (var i = 0; i < window._data_header.length; i++) {
		  var obj = {
		    name: window._data_header[i],
		    data: [],
		    marker: {
		      enabled: true,
		      symbol: "circle"
		    }
		  };
		  series.push(obj);
		}

		//for (var i = 0; i < window._data_value.length; i++) {
		for (var key in window._data_value) {
		  var item = window._data_value[key];
		  categories.push(key);
		  for (var j = 0; j < item.length; j++) {
		    var subitem = item[j];

		    if (subitem == '-') subitem = 0;
		    series[j]['data'].push(parseFloat(subitem));

		  }
		}

		// for (var i = 0; i < series.length; i++) {
		//   series[i]['data'].reverse();
		// }

		Highcharts.chart('container_chart', {
		  chart: {
		    type: 'spline'
		  },
		  title: {
		    text: ''
		  },
		  subtitle: {
		    text: 'Source: fuelcheck.nsw.gov.au'
		  },
		  xAxis: {
		    categories: categories,
		    // title: {
		    //   text: 'Date'
		    // }
		  },
		  yAxis: {
		    title: {
		      text: ''
		    }
		  },
		  plotOptions: {
		    spline: {
		      dataLabels: {
		        //enabled: true
		      },
		      enableMouseTracking: true
		    }
		  },
		  series: series,
		  tooltip: {
		    useHTML: true,
		    headerFormat: '<div class="chart-tooltip-container" style="border-top: {series.color} 4px solid;"><div style="text-align: center"><small>{point.key}</small></div>',
		    pointFormat: '<div class="chart-tooltip-name" style="color: {series.color};"><b>{series.name}</b></div><div class="chart-tooltip-value" ><b>{point.y}</b></div><div style="text-align: center"><i>cents per litre</i></div>',
		    footerFormat: '</div>',
		    borderColor: 'transparent',
		    backgroundColor: '#FFFFFF'
		  },
		  legend: {
		    enabled: true
		  },
		  colors: ['#748bff', '#c599e0', '#facc62', '#04cbca', '#6dd56f']
		});
	}

	// Petrol Page: Cover Background
	$('.cc-pp-single-cover').css('background-image', 'url('+$('.cc-pp-single-cover').attr('data-bg')+')');

	// Petrol Page: Slideshow
	$('.cc-pp__slide').flickity({
		prevNextButtons: false,
		autoPlay: false,
		pageDots: false
	});
	$('.cc-pp__slide-thumbs').flickity({
	  asNavFor: '.cc-pp__slide',
	  contain: true,
	  pageDots: false,
	  prevNextButtons: false
	});
});



var choices = [];
var arr = [];
var suburbFull;
var suburValue;
var postValue;
var submitReport;
jQuery(document).ready(function($) {
    var geoJson = '';
    if (typeof geodata !== 'undefined') {
        geoJson = geodata;
    } else {
        geoJson = "/"+jsfolder+"/js/geodata.json";
    }

    $.getJSON(geoJson, function(data) {
        $.each(data, function(name, value) {
            value[2] = value[2].length < 4 ? '0' + value[2] : value[2];
            arr.push("" + value.join(', '));
        });
        $('#postcode, #work_postcode').autoComplete({
            minChars: 1,
            source: function(term, suggest) {
                term = term.toLowerCase();
                var choices = arr;

                var matches = [];
                for (i = 0; i < choices.length; i++) {
                    if (~choices[i].toLowerCase().indexOf(term))
                        matches.push(choices[i]);
                }

                suggest(matches);
            },
            onSelect: function(e, term, item) {
                $(this).focus();
                var val = $('#postcode').val();
                $('.autocomplete-suggestions').hide();
                localStorage.has_selected_address = 1;
                localStorage.selected_address = val;
            },
            cache: false
        });
    });

    $("#postcode, #work_postcode").keydown(function(event) {
        if (event.keyCode == 13) {
            var val = $(this).val();
            localStorage.has_selected_address = 1;
            localStorage.selected_address = val;
        }
    });

    $("#postcode").change(function() {
        var val = $(this).val();
        if (typeof localStorage.selected_address !== 'undefined') {
            if (localStorage.selected_address == val) {
                localStorage.has_selected_address = 1;
                localStorage.selected_address = val;
            } else {
                localStorage.has_selected_address = 0;
            }
        }
    });
});