<?php
/**
 * WP Theme Header
 *
 * Displays all of the <head> section
 *
 * @package Himmelen
 */
global $himmelen_theme_options;

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// Demo settings
if ( defined('DEMO_MODE') && isset($_GET['header_logo_position']) ) {
  $himmelen_theme_options['header_logo_position'] = esc_html($_GET['header_logo_position']);
}
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>
<?php 
// Demo settings
if ( defined('DEMO_MODE') && isset($_GET['blog_layout']) ) {
  $himmelen_theme_options['blog_layout'] = $_GET['blog_layout'];
}

// Blog styles
$blog_style = 1;

$blog_style_class = 'blog-style-'.$blog_style;

// Slider on/off
if(isset($himmelen_theme_options['blog_enable_homepage_slider'])) {
  $blog_enable_homepage_slider = $himmelen_theme_options['blog_enable_homepage_slider'];
} else {
  $blog_enable_homepage_slider = false;
}

if($blog_enable_homepage_slider) {
  $blog_style_class .= ' blog-slider-enable';
} else {
  $blog_style_class .= ' blog-slider-disable';
}

?>
<body <?php echo body_class(esc_attr($blog_style_class)); ?>>

<?php do_action( 'before' ); ?>

<?php 

// Center logo
if(isset($himmelen_theme_options['header_logo_position'])) {
  $header_container_add_class = ' header-logo-'.$himmelen_theme_options['header_logo_position'];
} else {
  $header_container_add_class = '';
}
?>
<?php 
// Disable header
if(!isset($himmelen_theme_options['disable_header'])) {
  $himmelen_theme_options['disable_header'] = false;
}

if(isset($himmelen_theme_options['disable_header']) && !$himmelen_theme_options['disable_header']):
?>
<header>
<div class="container<?php echo esc_attr($header_container_add_class); ?>">
  <div class="row">
    <div class="col-md-12">
     
      <div class="header-left">
        <?php himmelen_header_left_show(); ?>
      </div>
      
      <div class="header-center">
        <?php himmelen_header_center_show(); ?>
      </div>

      <div class="header-right">
        <?php himmelen_header_right_show(); ?>
      </div>
    </div>
  </div>
    
</div>
<?php himmelen_menu_below_header_show(); ?>
</header>
<?php endif; ?>