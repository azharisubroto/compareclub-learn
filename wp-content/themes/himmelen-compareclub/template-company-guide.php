<?php
/**
 * Template Name: Company Guide
 * Template Post Type: post
 * @package compareclub
 */
get_header('compareclub');
?>

<section class="section-tos py-5">
	<div class="container">
		<div class="row">
			<!-- START CONTENT SIDE -->
			<div class="col-lg-9 col-md-7">
				<div class="card tos-content brd-0 rds-0">
					<div class="card-body px-2 px-md-5 py-3 py-md-5 text-14">
						<?php if( have_posts() ): ?>
							<?php while( have_posts() ): the_post(); ?>
								<div class="row mb-4 mb-md-4 align-items-center">
									<div class="col-md-8">
										<?php the_title( '<h3 class="text-24 text-secondary weight-medium mb-4">', '</h3>', true ); ?>
									</div>
									<div class="col-md-4 text-left text-md-right company-logo">
										<?php if( has_post_thumbnail() ): ?>	
											<?php the_post_thumbnail('full'); ?>							
										<?php endif; ?>
									</div>							
								</div>
						
								<?php the_content(); ?>
	
								<div class="accordion" id="guide-accordion">
									<?php 
										$accordions = get_field('accordion');
										$i = 1;
									 ?>
									<?php foreach( $accordions as $accordion ): ?>
										<?php
											$acc_id = str_replace(' ', '-', $accordion['title']);
										?>
										<div class="card mb-2">
											<div class="card-header" id="heading-<?php echo $i.'-'.$acc_id; ?>">
												<h5 class="mb-0">
													<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#<?php echo $acc_id.'-'.$i; ?>" aria-expanded="true" aria-controls="<?php echo $acc_id.'-'.$i; ?>">
														<?php echo $accordion['title']; ?>
													</button>
												</h5>
											</div>

											<div id="<?php echo $acc_id.'-'.$i; ?>" class="collapse show" aria-labelledby="heading-<?php echo $i.'-'.$acc_id; ?>">
												<div class="card-body">
													<?php echo do_shortcode( $accordion['content'] ); ?>
												</div>
											</div>
										</div>
									<?php $i++; endforeach; ?>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>
			<!-- END CONTENT SIDE -->

			<!-- SIDEBAR -->
			<div class="col-lg-3 col-md-5">
				<!-- Widget: search form -->
				<div class="cc-widget bg-secondary py-4 px-4 mb-4 rds-0">
					<form action="#">
						<input type="search" placeholder="Search" class="form-control cc-widget-search brd-0 rds-0">
					</form>
				</div>
				<!-- // End Widget: search form -->
				<!-- Widget: ready to save? -->
				<div class="cc-widget card rds-0 py-4 px-3 mb-5">
					<div class="row justify-content-center align-items-center">
						<div class="col-md-12 text-center text-md-right">
							<img src="<?php echo get_template_directory_uri();?>/img/readytosave.png" alt="">
						</div>
						<div class="col-md-12 text-center mt-4 mt-md-4">
							<div class="text-18 mb-3 weight-medium">Ready to save?</div>
							<a href="#" class="btn btn-primary text-18">Join the Club</a>
						</div>
					</div>
				</div>
				<!-- // END Widget: ready to save? -->
				<!-- Widget: Popular Posts -->
				<div class="cc-widget cc-popular-post">
					<h4>Popular Posts</h4>
					
					<?php 
						$args = array(
							'post_type' => 'post',
							'orderby' => 'rand',
							'posts_per_page' => 4
						);
						$posts = new WP_Query( $args );
					 ?>

					<?php if( $posts->have_posts() ): ?>
						<?php while( $posts->have_posts() ): $posts->the_post(); ?>
							<!-- Popular post item -->
							<div class="cc-popular-post--item">
								<div class="row xs">
									<div class="col-3">
										<div class="cover-square">
											<?php if( has_post_thumbnail() ): ?>	
												<?php the_post_thumbnail('thumbnail'); ?>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-9">
										<div class="text-secondary text-10 lh-1 mb-2"><?php the_time('l, F jS, Y') ?></div>
										<a href="<?php the_permalink(); ?>" class="text-dark weight-medium text-14"><?php the_title(); ?></a>
									</div>
								</div>
							</div>
							<!-- Popular post item -->
						<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</div>
				<!-- // END Widget: Popular Posts -->
			</div>
			<!-- END SIDEBAR -->
		</div>
	</div>
</section>

<?php get_footer('compareclub'); ?>