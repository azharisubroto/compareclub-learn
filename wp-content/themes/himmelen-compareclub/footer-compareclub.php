<?php
	// get meta tags from certain url
	if( $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'dev.compareclub.com.au' ) {
		$url = 'https://dev.compareclub.com.au';
	} else {
		$url = 'https://compareclub.com.au';
	}
	$tags = get_meta_tags( $url ); // get meta tags
	$footerurl = $url.'/blog-footer/?key=tYxLROVjWE028876udsmwJodW'; // set footer url
	$footer = file_get_contents( $footerurl ); // get footer content
?>

<!-- BEGIN Footer : <?php echo $tags['release-version']; ?> -->
<?php echo $footer; ?>
<!-- END Footer : <?php echo $tags['release-version']; ?> -->

<?php wp_footer(); ?>

</body>
</html>