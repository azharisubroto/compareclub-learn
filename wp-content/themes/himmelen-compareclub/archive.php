<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Himmelen
 */

get_header();

global $himmelen_theme_options;

$archive_sidebarposition = $himmelen_theme_options['archive_sidebar_position'];

if(is_active_sidebar( 'main-sidebar' ) && ($archive_sidebarposition <> 'disable') ) {
	$span_class = 'col-md-9';
}
else {
	$span_class = 'col-md-12';
}

// Blog layout
if(isset($himmelen_theme_options['blog_layout'])) {
	$blog_layout = $himmelen_theme_options['blog_layout'];
} else {
	$blog_layout = 'layout_default';
}

if($blog_layout == 'layout_masonry') {
	wp_register_script('masonry', get_template_directory_uri() . '/js/query.masonry.min.js');
	wp_enqueue_script('masonry');

	$blog_enable_masonry_design = true;
	$blog_masonry_class = ' blog-masonry-layout';
} else {
	$blog_enable_masonry_design = false;
	$blog_masonry_class = '';
}

?>
<?php if($blog_layout == 'layout_masonry'): ?>
<script>
(function($){
$(document).ready(function() {

	var $container = $('.blog-masonry-layout');
	$container.imagesLoaded(function(){
	  $container.masonry({
	    itemSelector : '.blog-masonry-layout .blog-post'
	  });
	});

});})(jQuery);
</script>
<?php endif; ?>
<div class="content-block">
<div class="container-fluid container-page-item-title">
	<div class="row">
	<div class="col-md-12">
	<div class="page-item-title-archive">
		
	      <?php
			if ( is_category() ) :
				echo '<p>'.__( 'Category', 'himmelen' ).'</p>';
				echo ( '<h1>' . single_cat_title( '', false ) . '</h1>' );

			elseif ( is_tag() ) :
				
				echo '<p>'.__( 'Tag', 'himmelen' ).'</p>';
				echo ( '<h1>' . single_tag_title( '', false ) . '</h1>' );

			elseif ( is_author() ) :
				/* Queue the first post, that way we know
				 * what author we're dealing with (if that is the case).
				*/
				the_post();
				echo '<p>'.__( 'Author', 'himmelen' ).'</p>';
				echo ( '<h1>' . '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' . '</h1>' );
				//printf( __( 'Author Archives: %s', 'himmelen' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' );
				/* Since we called the_post() above, we need to
				 * rewind the loop back to the beginning that way
				 * we can run the loop properly, in full.
				 */
				rewind_posts();

			elseif ( is_day() ) :
				//printf( __( 'Daily Archives: %s', 'himmelen' ), '<span>' . get_the_date() . '</span>' );
				echo '<p>'.__( 'Daily Archives', 'himmelen' ).'</p>';
				echo ( '<h1>' . get_the_date() . '</h1>' );

			elseif ( is_month() ) :
				//printf( __( 'Monthly Archives: %s', 'himmelen' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
				echo '<p>'.__( 'Monthly Archives', 'himmelen' ).'</p>';
				echo ( '<h1>' . get_the_date( 'F Y' ) . '</h1>' );

			elseif ( is_year() ) :
				//printf( __( 'Yearly Archives: %s', 'himmelen' ), '<span>' . get_the_date( 'Y' ) . '</span>' );
				echo '<p>'.__( 'Yearly Archives', 'himmelen' ).'</p>';
				echo ( '<h1>' . get_the_date( 'Y' ) . '</h1>' );

			elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
				//esc_html_e( 'Asides', 'himmelen' );
				echo '<p>'.__( 'Post format', 'himmelen' ).'</p>';
				echo ( '<h1>' . esc_html_e( 'Aside', 'himmelen' ) . '</h1>' );

			elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
				//esc_html_e( 'Images', 'himmelen');
				echo '<p>'.__( 'Post format', 'himmelen' ).'</p>';
				echo ( '<h1>' . esc_html_e( 'Images', 'himmelen' ) . '</h1>' );

			elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
				//esc_html_e( 'Videos', 'himmelen' );
				echo '<p>'.__( 'Post format', 'himmelen' ).'</p>';
				echo ( '<h1>' . esc_html_e( 'Videos', 'himmelen' ) . '</h1>' );

			elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
				//esc_html_e( 'Quotes', 'himmelen' );
				echo '<p>'.__( 'Post format', 'himmelen' ).'</p>';
				echo ( '<h1>' . esc_html_e( 'Quotes', 'himmelen' ) . '</h1>' );

			elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
				//esc_html_e( 'Links', 'himmelen' );
				echo '<p>'.__( 'Post format', 'himmelen' ).'</p>';
				echo ( '<h1>' . esc_html_e( 'Links', 'himmelen' ) . '</h1>' );

			else :
				//esc_html_e( 'Archives', 'himmelen' );
				echo '<p>'.__( 'Posts', 'himmelen' ).'</p>';
				echo ( '<h1>' . esc_html_e( 'Archives', 'himmelen' ) . '</h1>' );

			endif;
		?>

	</div>
	</div>
	</div>
</div>
<div class="container">
	<div class="row">
<?php if ( is_active_sidebar( 'main-sidebar' ) && ( $archive_sidebarposition == 'left')) : ?>
		<div class="col-md-3 main-sidebar sidebar">
		<ul id="main-sidebar">
		  <?php dynamic_sidebar( 'main-sidebar' ); ?>
		</ul>
		</div>
		<?php endif; ?>
		<div class="<?php echo esc_attr($span_class); ?>">
		<?php
			if ( is_category() ) :
				// show an optional category description
				$category_description = category_description();
				if ( ! empty( $category_description ) ) :
					echo '<div class="container-fluid category-description">
		<div class="row">
		<div class="col-md-12">'.apply_filters( 'category_archive_meta', '<div class="taxonomy-description">' . wp_kses_post($category_description) . '</div>' ).'		</div>
		</div>
		</div>';
				endif;

			elseif ( is_tag() ) :
				// show an optional tag description
				$tag_description = tag_description();
				if ( ! empty( $tag_description ) ) :
					echo '<div class="container-fluid category-description">
		<div class="row">
		<div class="col-md-12">'.apply_filters( 'tag_archive_meta', '<div class="taxonomy-description">' . wp_kses_post($tag_description) . '</div>' ).'		</div>
		</div>
		</div>';
				endif;

			endif;
		?>
		<div class="blog-posts-list<?php echo esc_attr($blog_masonry_class);?>">
		
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
					?>

				<?php endwhile; ?>
				
				
				
			<?php else : ?>

				<?php get_template_part( 'no-results', 'archive' ); ?>

			<?php endif; ?>
		</div>
		<?php himmelen_content_nav( 'nav-below' ); ?>
		</div>
		<?php if ( is_active_sidebar( 'main-sidebar' ) && ( $archive_sidebarposition == 'right')) : ?>
		<div class="col-md-3 main-sidebar sidebar">
		<ul id="main-sidebar">
		  <?php dynamic_sidebar( 'main-sidebar' ); ?>
		</ul>
		</div>
		<?php endif; ?>
	</div>
</div>
</div>
<?php get_footer(); ?>