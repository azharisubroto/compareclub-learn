<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<style>
		.cover-square {
			overflow: hidden;
			width: 100%;
			height: 60px;
		}
		
		.cover-square img {
			width: 100%;
    		height: 100%!important;
    		object-fit: cover;
		}
		
		.btn, .cc-mega-menu h4 {
			font-family:"Gotham", sans-serif!important;
		}
	</style>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php
	if( $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'dev.compareclub.com.au' ) {
		$url = 'https://dev.compareclub.com.au';
	} else {
		$url = 'https://compareclub.com.au';
	}
	$tags = get_meta_tags( $url ); // get meta tags
	$headerurl = $url.'/blog-header/?key=tYxLROVjWE028876udsmwJodW'; // set header blog url
	$header = file_get_contents( $headerurl ); // get header content
?>

<!-- BEGIN MENU : <?php echo $tags['release-version']; ?> -->
<?php echo $header; ?>
<!-- END MENU : <?php echo $tags['release-version']; ?> -->