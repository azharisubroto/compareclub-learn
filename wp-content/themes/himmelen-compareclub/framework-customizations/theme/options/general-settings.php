<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general' => array(
		'title'   => __( 'Header', 'compareclub' ),
		'type'    => 'tab',
		'options' => array(
			'Header-box' => array(
				'title'   => __( 'Header Settings', 'compareclub' ),
				'type'    => 'box',
				'options' => array(
					'hic_link' => array(
						'label' => __( 'hic link', 'compareclub' ),
						'type'  => 'text',
					),
					'hl_link' => array(
						'label' => __( 'hl link', 'compareclub' ),
						'type'  => 'text',
					),
					'lic_link' => array(
						'label' => __( 'lic link', 'compareclub' ),
						'type'  => 'text',
					),
					'gro_link' => array(
						'label' => __( 'gro link', 'compareclub' ),
						'type'  => 'text',
					),
					'eng_link' => array(
						'label' => __( 'eng link', 'compareclub' ),
						'type'  => 'text',
					),
					'ib_link' => array(
						'label' => __( 'ib link', 'compareclub' ),
						'type'  => 'text',
					),
					'cc_link' => array(
						'label' => __( 'cc link', 'compareclub' ),
						'type'  => 'text',
					),
					'pp_link' => array(
						'label' => __( 'pp link', 'compareclub' ),
						'type'  => 'text',
					),
				)
			),
		)
	)
);