<?php
/**
 * @package Himmelen
 */
global $himmelen_theme_options, $wp_embed;

$js_script = '';

if(!isset($himmelen_theme_options['blog_post_hide_featured_image'])) {
	$himmelen_theme_options['blog_post_hide_featured_image'] = false;
} 

$post_classes = get_post_class();

$current_post_format = $post_classes[4];

$post_formats_media = array('format-audio', 'format-video', 'format-gallery');

$post_sidebarposition = get_post_meta( get_the_ID(), '_post_sidebarposition_value', true );
$post_socialshare_disable = get_post_meta( get_the_ID(), '_post_socialshare_disable_value', true );
$post_disable_featured_image = get_post_meta( get_the_ID(), '_post_disable_featured_image_value', true );

// Demo settings
if ( defined('DEMO_MODE') && isset($_GET['post_sidebar_position']) ) {
  $himmelen_theme_options['post_sidebar_position'] = $_GET['post_sidebar_position'];
}

if(!isset($himmelen_theme_options['post_sidebar_position'])) {
	$himmelen_theme_options['post_sidebar_position'] = 'disable';
}

if(!isset($post_sidebarposition)||($post_sidebarposition == '')) {
	$post_sidebarposition = 0;
}

if($post_sidebarposition == "0") {
	$post_sidebarposition = $himmelen_theme_options['post_sidebar_position'];
}

if(is_active_sidebar( 'main-sidebar' ) && ($post_sidebarposition <> 'disable') ) {
	$span_class = 'col-md-9';
}
else {
	$span_class = 'col-md-12 post-single-content';
}

// Post media
$post_embed_video = get_post_meta( get_the_ID(), '_himmelen_video_embed', true );

if($post_embed_video !== '') {
	$post_embed_video_output = $wp_embed->run_shortcode('[embed width="auto" height="660"]'.$post_embed_video.'[/embed]');

} else {
	$post_embed_video_output = '';
}

$post_embed_audio = get_post_meta( get_the_ID(), '_himmelen_audio_embed', true );

if($post_embed_audio !== '') {
	$post_embed_audio_output = $wp_embed->run_shortcode('[embed]'.$post_embed_audio.'[/embed]');

} else {
	$post_embed_audio_output = '';
}

$gallery_images_data = himmelen_cmb2_get_images_src( get_the_ID(), '_himmelen_gallery_file_list', 'blog-thumb' );

$header_background_image = get_post_meta( get_the_ID(), '_himmelen_header_image', true );

if(isset($header_background_image) && ($header_background_image!== '')) {
	$header_background_image_style = 'background-image: url('.$header_background_image.');';
	$header_background_class = ' with-bg';
} else {
	$header_background_image_style = '';
	$header_background_class = '';
}

if($gallery_images_data !== '') {

	$post_gallery_id = 'blog-post-gallery-'.get_the_ID();
	$post_embed_gallery_output = '<div class="blog-post-gallery-wrapper" id="'.$post_gallery_id.'" style="display: none;">';

	foreach ($gallery_images_data as $gallery_image) {
		$post_embed_gallery_output .= '<div class="blog-post-gallery-image"><a href="'.esc_url($gallery_image).'" rel="lightbox" title="'.get_the_title().'"><img src="'.esc_url($gallery_image).'" alt="'.get_the_title().'"/></a></div>';
	}

	$post_embed_gallery_output .= '</div>';

	$js_script = '<script>(function($){
            $(document).ready(function() {

                $("#'.$post_gallery_id.'").owlCarousel({
                    items: 1,
                    itemsDesktop:   [1199,1],
                    itemsDesktopSmall: [979,1],
                    itemsTablet: [768,1],
                    itemsMobile : [479,1],
                    autoPlay: true,
                    autoHeight: true,
                    navigation: true,
                    navigationText : false,
                    pagination: false,
                    afterInit : function(elem){
                        $(this).css("display", "block");
                    }
                });
            
            });})(jQuery);</script>';
	
} else {
	$post_embed_gallery_output = '';
}
?>

<div class="content-block">
<div class="container-fluid container-page-item-title<?php echo esc_attr($header_background_class); ?>" data-style="<?php echo esc_attr($header_background_image_style); ?>">
	<div class="row">
	<div class="col-md-12">
	<div class="page-item-title-single">
		<?php
		$categories_list = get_the_category_list(  ', '  );
		if ( $categories_list ) :
		?>
	    <div class="post-categories"><?php printf( __( '%1$s', 'himmelen' ), $categories_list ); ?></div>
	    <?php endif; // End if categories ?>
	    <h1><?php the_title(); ?></h1>
		<div class="post-date"><?php the_time(get_option( 'date_format' ));  ?></div> 
	</div>
	</div>
	</div>
</div>
<div class="post-container container">
	<div class="row">
<?php if ( is_active_sidebar( 'main-sidebar' ) && ( $post_sidebarposition == 'left')) : ?>
		<div class="col-md-3 main-sidebar sidebar">
		<ul id="main-sidebar">
		  <?php dynamic_sidebar( 'main-sidebar' ); ?>
		</ul>
		</div>
		<?php endif; ?>
		<div class="<?php echo esc_attr($span_class); ?>">
			<div class="blog-post blog-post-single clearfix">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="post-content-wrapper">
					
								<div class="post-content">
									<?php 
									if ( has_post_thumbnail()&&!in_array($current_post_format, $post_formats_media)&&(!$post_disable_featured_image ) ): // check if the post has a Post Thumbnail assigned to it.
									?>
									<div class="blog-post-thumb">
									
										<?php if(isset($himmelen_theme_options['blog_post_hide_featured_image']) && !$himmelen_theme_options['blog_post_hide_featured_image']):?>
										<?php the_post_thumbnail('blog-thumb'); ?>
										<?php endif; ?>
									
									</div>
									<?php endif; ?>
									<?php

									if(in_array($current_post_format, $post_formats_media)) {
										echo '<div class="blog-post-thumb">';

									// Post media
									if($current_post_format == 'format-video') {
										echo '<div class="blog-post-media blog-post-media-video">';
										echo ($post_embed_video_output);// escaping does not needed here, wordpress OEMBED function used for this var
										echo '</div>';
									}
									elseif($current_post_format == 'format-audio') {
										echo '<div class="blog-post-media blog-post-media-audio">';
										echo ($post_embed_audio_output);// escaping does not needed here, wordpress OEMBED function used for this var
										echo '</div>';
									}
									elseif($current_post_format == 'format-gallery') {
										echo '<div class="blog-post-media blog-post-media-gallery">';
										echo wp_kses_post($post_embed_gallery_output);
										echo '</div>';

										echo ($js_script);// this var is safe and can't be escaped via wordpress functions, because contain js+jquery+html, escaping will break the code here
									}
										echo '</div>';
									}
									?>									
									<?php if ( is_search() ) : // Only display Excerpts for Search ?>
									<div class="entry-summary">
										<?php the_excerpt(); ?>
									</div><!-- .entry-summary -->
									<?php else : ?>
									<div class="entry-content">
										<?php the_content('<div class="more-link">'.__( 'Continue reading...', 'himmelen' ).'</div>' ); ?>
										<?php
											wp_link_pages( array(
												'before' => '<div class="page-links">' . __( 'Pages:', 'himmelen' ),
												'after'  => '</div>',
											) );
										?>
									</div><!-- .entry-content -->
									<?php
										/* translators: used between list items, there is a space after the comma */
										$tags_list = get_the_tag_list( '', ''  );
										if ( $tags_list ) :
									?>
									
									<span class="tags">
										<?php echo wp_kses_post($tags_list); ?>
									</span>
									
									<?php endif; // End if $tags_list ?>
									<div class="post-info clearfix">
										<?php if(isset($himmelen_theme_options['blog_post_show_author'])&&($himmelen_theme_options['blog_post_show_author'])): ?>
										<div class="post-author"><?php esc_html_e('by','himmelen'); ?> <?php the_author();?></div>
										<?php endif; ?>
										
										<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>

										<div class="comments-count"><?php comments_popup_link( __( 'Leave a comment', 'himmelen' ), __( '1 Comment', 'himmelen' ), __( '% Comments', 'himmelen' ) ); ?></div>
										<?php endif; ?>

										
										<?php if(!isset($post_socialshare_disable) || !$post_socialshare_disable): ?>
											<div class="share-post">
											<?php get_template_part( 'share-post' ); ?>
											</div>
										<?php endif; ?>
									
									</div>
									
									<?php endif; ?>
									</div>
					
							</div>
				
				

			
				

				</article>

				
			</div>
			
			<?php if(isset($himmelen_theme_options['blog_enable_author_info'])&&($himmelen_theme_options['blog_enable_author_info'])): ?>
				<?php if ( is_single() && get_the_author_meta( 'description' ) ) : ?>
					<?php get_template_part( 'author-bio' ); ?>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php 
			if(isset($himmelen_theme_options['blog_post_navigation']) && $himmelen_theme_options['blog_post_navigation']) {
				himmelen_content_nav( 'nav-below' ); 
			}
			?>

			<?php if(isset($himmelen_theme_options['blog_post_show_related'])&&($himmelen_theme_options['blog_post_show_related'])): ?>
			<?php get_template_part( 'related-posts' ); ?>
			<?php endif; ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) 
					
					comments_template();
			?>
			
		</div>
		<?php if ( is_active_sidebar( 'main-sidebar' ) && ( $post_sidebarposition == 'right')) : ?>
		<div class="col-md-3 main-sidebar sidebar">
		<ul id="main-sidebar">
		  <?php dynamic_sidebar( 'main-sidebar' ); ?>
		</ul>
		</div>
		<?php endif; ?>
	</div>
	</div>
</div>
