<?php
/**
 * SETTINGS TAB
 **/
$ipanel_himmelen_tabs[] = array(
	'name' => 'Main Settings',
	'id' => 'main_settings'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "main_settings"
);

$ipanel_himmelen_option[] = array(
	"name" => "Enable theme CSS3 animations",
	"id" => "enable_theme_animations",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Enable colors and background colors fade effects",
	"type" => "checkbox",
);

$ipanel_himmelen_option[] = array(
	"name" => "<span style='color:red;font-weight: bold;'>Enable Coming soon/Maintenance mode</span>",
	"id" => "enable_comingsoon",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "WARNING: If you enable this option only admin will see website frontend. All regular visitors will see your coming soon page, that you need to create in 'Pages > Add page' and select 'Coming soon' template for 'Page Template' option. Check Theme Documentation for more details.",
	"type" => "checkbox",
);

$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);
/**
 * Header TAB
 **/
$ipanel_himmelen_tabs[] = array(
	'name' => 'Header',
	'id' => 'header_settings'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "header_settings"
);
$ipanel_himmelen_option[] = array(
	"name" => "Header layout",   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_himmelen_option[] = array(
	"name" => "Body Background image or pattern",
	"id" => "body_bg_image",
	"field_options" => array(
		"std" => ''
	),
	"desc" => "Upload your site body background image if you want to show it. Remove image to remove background.",
	"type" => "qup",
);
$ipanel_himmelen_option[] = array(
	"name" => "Body Background image behaviour",
	"id" => "body_bg_style",
	"std" => "cover",
	"options" => array(
		"none" => "Default",
		"repeat" => "Repeat (for pattern)",
		"cover" => "Cover (for large image)"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "Header Background image or pattern",
	"id" => "header_bg_image",
	"field_options" => array(
		"std" => ''
	),
	"desc" => "Upload your site header background image if you want to show it. Remove image to remove background.",
	"type" => "qup",
);
$ipanel_himmelen_option[] = array(
	"name" => "Header Background image behaviour",
	"id" => "header_bg_style",
	"std" => "cover",
	"options" => array(
		"none" => "Default",
		"repeat" => "Repeat (for pattern)",
		"cover" => "Cover (for large image)"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "Disable Header",
	"id" => "disable_header",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "This option will disable ALL header (with menu below header, logo, etc). Useful for minimalistic themes with left/right sidebar used to show logo and menu.",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Header height in pixels",
	"id" => "header_height",
	"std" => "144",
	"desc" => "Default: 144",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Disable search in header menu",
	"id" => "disable_top_menu_search",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "This option will disable search form in header menu",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Disable text tagline in header",
	"id" => "disable_header_tagline",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "This option will disable text tagline in header",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"type" => "htmlpage",
	"name" => '<div class="ipanel-label">
    <label>Logo upload</label>
  </div><div class="ipanel-input">
    You can upload your website logo in <a href="customize.php" target="_blank">WordPress Customizer</a> (in "Header Image" section at the left sidebar).<br/><br/><br/>
  </div>'
);
$ipanel_himmelen_option[] = array(
	"name" => "Logo width (px)",
	"id" => "logo_width",
	"std" => "260",
	"desc" => "Default: 260. Upload retina logo (2x size) and input your regular logo width here. For example if your retina logo have 400px width put 200 value here. If you does not use retina logo input regular logo width here (your logo image width).",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Sticky/Fixed top header (with menu, search, social icons)",
	"id" => "enable_sticky_header",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Top Header will be fixed to top if enabled",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Enable left side offcanvas floating sidebar menu",
	"id" => "enable_offcanvas_sidebar",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Sidebar can be opened by toggle button near header mini cart. You can add widgets to this sidebar in 'Offcanvas Right sidebar' in Appearance > Widgets",
	"type" => "checkbox",
);

$ipanel_himmelen_option[] = array(
	"name" => "MainMenu font decoration",
	"id" => "header_menu_font_decoration",
	"std" => "uppercase",
	"options" => array(
		"uppercase" => "Uppercase letters",
		"italic" => "Italic letters",
		"none" => "None",
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "MainMenu font size",
	"id" => "header_menu_font_size",
	"std" => "normalfont",
	"options" => array(
		"largefont" => "Large font",
		"normalfont" => "Normal font"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "MainMenu dropdown arrows style for submenus",
	"id" => "header_menu_arrow_style",
	"std" => "downarrow",
	"options" => array(
		"rightarrow" => "Right arrow",
		"downarrow" => "Down arrow",
		"noarrow" => "Disable arrow"
	),
	"desc" => "",
	"type" => "select",
);

$ipanel_himmelen_option[] = array(
	"name" => "Header Logo position",   
	"id" => "header_logo_position",
	"options" => array(
		'left' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_1.png',
			"label" => 'Left'
		),
		'center' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_2.png',
			"label" => 'Center'
		),
		'right' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_3.png',
			"label" => 'Right'
		),
	),
	"std" => "center",
	"desc" => "",
	"type" => "image",
);
$ipanel_himmelen_option[] = array(
	"name" => "Header Social Icons position",   
	"id" => "header_social_position",
	"options" => array(
		'left' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_1.png',
			"label" => 'Left'
		),
		'center' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_2.png',
			"label" => 'Center'
		),
		'right' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_3.png',
			"label" => 'Right'
		),
		'disable' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_0.png',
			"label" => 'Disable'
		)
	),
	"std" => "left",
	"desc" => "",
	"type" => "image",
);
$ipanel_himmelen_option[] = array(
	"name" => "Header Banner position",   
	"id" => "header_banner_position",
	"options" => array(
		'left' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_1.png',
			"label" => 'Left'
		),
		'center' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_2.png',
			"label" => 'Center'
		),
		'right' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_3.png',
			"label" => 'Right'
		),
		'disable' => array(
			"image" => IPANEL_URI . 'option-images/header_block_position_0.png',
			"label" => 'Disable'
		)
	),
	"std" => "disable",
	"desc" => "You can show banner image or some text in your header. Make sure that you use different positions for logo and your banner (for example logo at the left and banner at the right).",
	"type" => "image",
);
$ipanel_himmelen_option[] = array(
	"name" => "Header Banner content",
	"id" => "header_banner_editor",
	"std" => '<a class="btn button" href="#" target="_blank">SUBSCRIBE</a>',
	"desc" => "If you selected Header banner position below you can use any HTML here to show your banner or other content in header.",
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_himmelen_option[] = array(
		"type" => "EndSection"
);
$ipanel_himmelen_option[] = array(
	
	"name" => "Social icons",   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_himmelen_option[] = array(
	"type" => "info",
	"name" => "Leave URL fields blank to hide this social icons",
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_himmelen_option[] = array(
	"name" => "Facebook Page url",
	"id" => "facebook",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Vkontakte page url",
	"id" => "vk",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Twitter Page url",
	"id" => "twitter",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Google+ Page url",
	"id" => "google-plus",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "LinkedIn Page url",
	"id" => "linkedin",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Dribbble Page url",
	"id" => "dribbble",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Behance Page url",
	"id" => "behance",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Instagram Page url",
	"id" => "instagram",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Tumblr page url",
	"id" => "tumblr",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Pinterest page url",
	"id" => "pinterest",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Vimeo page url",
	"id" => "vimeo-square",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "YouTube page url",
	"id" => "youtube",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Skype url",
	"id" => "skype",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Flickr url",
	"id" => "flickr",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "RSS url",
	"id" => "rss",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Deviantart url",
	"id" => "deviantart",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "500px url",
	"id" => "500px",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Etsy url",
	"id" => "etsy",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Telegram url",
	"id" => "telegram",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Odnoklassniki url",
	"id" => "odnoklassniki",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Houzz url",
	"id" => "houzz",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Slack url",
	"id" => "slack",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "QQ url",
	"id" => "qq",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
		"type" => "EndSection"
);
$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);
/**
 * FOOTER TAB
 **/
$ipanel_himmelen_tabs[] = array(
	'name' => 'Footer',
	'id' => 'footer_settings'
);
$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "footer_settings"
);
$ipanel_himmelen_option[] = array(
	"name" => "Show 'Footer light sidebar' only on homepage",
	"id" => "footer_sidebar_1_homepage_only",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Footer copyright text",
	"id" => "footer_copyright_editor",
	"std" => "Powered by <a href='http://themeforest.net/user/dedalx/' target='_blank'>Himmelen - Premium Wordpress Theme</a>",
	"desc" => "",
	"field_options" => array(
		'media_buttons' => false
	),
	"type" => "wp_editor",
);

$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);

/**
 * SIDEBARS TAB
 **/
$ipanel_himmelen_tabs[] = array(
	'name' => 'Sidebars',
	'id' => 'sidebar_settings'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "sidebar_settings"
);

$ipanel_himmelen_option[] = array(
	"name" => "Blog page sidebar position",   
	"id" => "blog_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => 'Left'
		),
		'right' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => 'Right'
		),
		'disable' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => 'Disable sidebar'
		),
	),
	"std" => "disable",
	"desc" => "",
	"type" => "image",
);

$ipanel_himmelen_option[] = array(
	"name" => "Pages sidebar position",   
	"id" => "page_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => 'Left'
		),
		'right' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => 'Right'
		),
		'disable' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => 'Disable sidebar'
		),
	),
	"std" => "disable",
	"desc" => "",
	"type" => "image",
);

$ipanel_himmelen_option[] = array(
	"name" => "Blog Archive page sidebar position",   
	"id" => "archive_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => 'Left'
		),
		'right' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => 'Right'
		),
		'disable' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => 'Disable sidebar'
		),
	),
	"std" => "right",
	"desc" => "",
	"type" => "image",
);

$ipanel_himmelen_option[] = array(
	"name" => "Blog Search page sidebar position",   
	"id" => "search_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => 'Left'
		),
		'right' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => 'Right'
		),
		'disable' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => 'Disable sidebar'
		),
	),
	"std" => "right",
	"desc" => "",
	"type" => "image",
);

$ipanel_himmelen_option[] = array(
	"name" => "Blog post sidebar position",   
	"id" => "post_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => 'Left'
		),
		'right' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => 'Right'
		),
		'disable' => array(
			"image" => IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => 'Disable sidebar'
		),
	),
	"std" => "disable",
	"desc" => "",
	"type" => "image",
);

$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);
/**
 * BLOG TAB
 **/
$ipanel_himmelen_tabs[] = array(
	'name' => 'Blog',
	'id' => 'blog_settings'
);
$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "blog_settings"
);
$ipanel_himmelen_option[] = array(
	"name" => "Main Blog settings",   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_himmelen_option[] = array(
	"name" => "Blog layout",   
	"id" => "blog_layout",
	"options" => array(
		'layout_default' => array(
			"image" => IPANEL_URI . 'option-images/blog_layout_1.png',
			"label" => 'Default layout'
		),
		'layout_vertical_design' => array(
			"image" => IPANEL_URI . 'option-images/blog_layout_2.png',
			"label" => 'Show every third post in vertical design'
		),
		'layout_2column_design' => array(
			"image" => IPANEL_URI . 'option-images/blog_layout_3.png',
			"label" => 'Show second and next posts in 2 columns'
		),
		'layout_list' => array(
			"image" => IPANEL_URI . 'option-images/blog_layout_4.png',
			"label" => 'List with short posts blocks'
		),
		'layout_masonry' => array(
			"image" => IPANEL_URI . 'option-images/blog_layout_5.png',
			"label" => 'Masonry layout'
		),
	),
	"std" => "layout_default",
	"desc" => "This option will completely change blog listing layout and posts display.",
	"type" => "image",
);
$ipanel_himmelen_option[] = array(
	"name" => "Show blog posts in listing as",
	"id" => "blog_post_loop_type",
	"std" => "content",
	"options" => array(
		"content" => "Full content (You will add More tag manually)",
		"excerpt" => "Excerpt (Auto crop by words)",
	),
	"desc" => "We recommend you to use Fullwidth layout for Slider Style 3",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "Post excerpt length (words)",
	"id" => "post_excerpt_legth",
	"std" => "40",
	"desc" => "Used by WordPress for post shortening. Default: 40",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Show author name ('by author') in blog posts",
	"id" => "blog_post_show_author",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Show related posts on posts listing page",
	"id" => "blog_list_show_related",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Will show 3 related posts after every post in posts list. Does not available in Masonry layout and 2 column layout.",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
		"type" => "EndSection"
);

$ipanel_himmelen_option[] = array(
	"name" => "Featured Posts slider settings",   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_himmelen_option[] = array(
	"name" => "Show Featured posts slider on homepage",
	"id" => "blog_enable_homepage_slider",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "You can mark posts as featured in post edit screen at the bottom settings box to display it in slider in homepage header.",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Featured posts block style",   
	"id" => "blog_homepage_slider_style",
	"options" => array(
		'1' => array(
			"image" => IPANEL_URI . 'option-images/posts_slider_1.png',
			"label" => 'Style 1 - Simple Post Slider'
		),
		'2' => array(
			"image" => IPANEL_URI . 'option-images/posts_slider_2.png',
			"label" => 'Style 2 - Advanced Post Slider'
		),
	),
	"std" => "2",
	"desc" => "You can change how will look your posts slider with predefined layouts and styles.</ul>",
	"type" => "image",
);
$ipanel_himmelen_option[] = array(
	"name" => "Featured posts slider width",
	"id" => "blog_homepage_slider_fullwidth",
	"std" => "0",
	"options" => array(
		"1" => "Fullwidth",
		"0" => "Boxed",
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "Featured posts slider autoplay",
	"id" => "blog_homepage_slider_autoplay",
	"std" => "1",
	"options" => array(
		"1" => "Enable",
		"0" => "Disable",
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "Featured posts slider navigation arrows",
	"id" => "blog_homepage_slider_navigation",
	"std" => "1",
	"options" => array(
		"1" => "Enable",
		"0" => "Disable",
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "Featured posts slider pagination buttons",
	"id" => "blog_homepage_slider_pagination",
	"std" => "1",
	"options" => array(
		"1" => "Enable",
		"0" => "Disable",
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
		"type" => "EndSection"
);
$ipanel_himmelen_option[] = array(
	"name" => "Popular Posts Homepage Block settings",   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_himmelen_option[] = array(
	"name" => "Show Popular Posts Block on homepage footer",
	"id" => "blog_enable_homepage_popular_posts",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Show Popular Posts in Homepage footer. Popular posts - posts with maximum views.",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Popular posts block title",
	"id" => "blog_homepage_popular_posts_title",
	"std" => "Popular posts",
	"desc" => "Change default Popular Posts block title. Leave empty to hide title.",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Popular posts block layout",
	"id" => "blog_homepage_popular_posts_layout",
	"std" => "masonry",
	"options" => array(
		"masonry" => "Masonry",
		"small" => "Small Blocks",
		"large" => "Large Blocks",
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "Popular posts limit (rows)",
	"id" => "blog_homepage_popular_posts_limit",
	"std" => "2",
	"options" => array(
		"1" => "One row",
		"2" => "Two rows",
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"name" => "Popular posts category slug",
	"id" => "blog_homepage_popular_posts_category",
	"std" => "",
	"desc" => "If you want to show popular posts only from some category specify it's SLUG here (You can create special category like 'Popular' and assing posts to it if you want to show only selected posts). You can see/set category SLUG when you edit category. Leave empty to show posts from all categories.",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
		"type" => "EndSection"
);
$ipanel_himmelen_option[] = array(
	"name" => "Homepage Welcome Block",   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_himmelen_option[] = array(
	"name" => "Show Homepage Welcome Left block",
	"id" => "blog_enable_homepage_welcome_block_left",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "You can display any HTML content in this block below your slider or header. This block will work only if you enabled Center block.",
	"type" => "checkbox",
);

$ipanel_himmelen_option[] = array(
	"name" => "Homepage Welcome Left block content",
	"id" => "blog_homepage_welcome_block_left_content",
	"std" => '<a href="#"><img class="alignnone size-full wp-image-1718" src="http://wp.magnium-themes.com/himmelen/himmelen-1/wp-content/uploads/2015/09/welcome-left-image.jpg" alt="welcome-left-image"/></a><h3>Travel category</h3><p class="welcome-block-text">The concept of home is shaped by things with meaning and love</p>',
	"desc" => "You can use any HTML here to display any content in your welcome block with predefined layout.",
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);


$ipanel_himmelen_option[] = array(
	"name" => "Show Homepage Welcome Center block ",
	"id" => "blog_enable_homepage_welcome_block_center",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "You can display any HTML content in this block below your slider or header with any layout.",
	"type" => "checkbox",
);

$ipanel_himmelen_option[] = array(
	"name" => "Homepage Welcome Center block content",
	"id" => "blog_homepage_welcome_block_center_content",
	"std" => '<img class="size-full wp-image-1721 alignleft" src="http://wp.magnium-themes.com/himmelen/himmelen-1/wp-content/uploads/2015/09/welcome-center-image.jpg" alt="welcome-center-image" width="166" height="208" /><h5>About me</h5><h3>Hi to everyone.<br class="welcome-block-br"/> My name is Anna!</h3><p class="welcome-block-text">Hello, my name is mani! If you are bloggers, travelers, photographers, illustrators, food lovers or simply you have something important to tell - you are in most beautiful place</p><a class="btn" href="#">Read more</a>',
	"desc" => "You can use any HTML here to display any content in your welcome block with any layout.",
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);

$ipanel_himmelen_option[] = array(
	"name" => "Show Homepage Right Welcome block ",
	"id" => "blog_enable_homepage_welcome_block_right",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "You can display any HTML content in this block below your slider or header with any layout. This block will work only if you enabled Center block.",
	"type" => "checkbox",
);

$ipanel_himmelen_option[] = array(
	"name" => "Homepage Welcome Right block content",
	"id" => "blog_homepage_welcome_block_right_content",
	"std" => '<div class="text-center"><h5>Inspiration</h5><h3>Inspiration<br/>category</h3><p class="welcome-block-text">Coming from two<br class="welcome-block-br"/> different cultures has<br class="welcome-block-br"/> been instrumental in<br class="welcome-block-br"/> how we’ve approached<br class="welcome-block-br"/> our design process</p><a class="btn" href="#">Read more</a></div>',
	"desc" => "You can use any HTML here to display any content in your welcome block with any layout.",
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);

$ipanel_himmelen_option[] = array(
		"type" => "EndSection"
);

$ipanel_himmelen_option[] = array(
	"name" => "Single Post page settings",   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_himmelen_option[] = array(
	"name" => "Show author info and avatar after single blog post",
	"id" => "blog_enable_author_info",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Show related posts on single post page",
	"id" => "blog_post_show_related",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Hide post featured image on single post page",
	"id" => "blog_post_hide_featured_image",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Enable this if you don't want to see featured post image on single post page.",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Show prev/next posts navigation links on single post page",
	"id" => "blog_post_navigation",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
		"type" => "EndSection"
);
$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);

/**
 * FONTS TAB
 **/

$ipanel_himmelen_tabs[] = array(
	'name' => 'Fonts',
	'id' => 'font_settings'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "font_settings"
);

$ipanel_himmelen_option[] = array(
	"name" => "Headers font",
	"id" => "header_font",
	"desc" => "Font used in headers. Default: Playfair Display",
	"options" => array(
		"font-sizes" => array(
			" " => "Font Size",
			'11' => '11px',
			'12' => '12px',
			'13' => '13px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			'17' => '17px',
			'18' => '18px',
			'19' => '19px',
			'20' => '20px',
			'21' => '21px',
			'22' => '22px',
			'23' => '23px',
			'24' => '24px',
			'25' => '25px',
			'26' => '26px',
			'27' => '27px',
			'28' => '28px',
			'29' => '29px',
			'30' => '30px',
			'31' => '31px',
			'32' => '32px',
			'33' => '33px',
			'34' => '34px',
			'35' => '35px',
			'36' => '36px',
			'37' => '37px',
			'38' => '38px',
			'39' => '39px',
			'40' => '40px',
			'41' => '41px',
			'42' => '42px',
			'43' => '43px',
			'44' => '44px',
			'45' => '45px',
			'46' => '46px',
			'47' => '47px',
			'48' => '48px',
			'49' => '49px',
			'50' => '50px'
		),
		"color" => false,
		"font-families" => iPanel::getGoogleFonts(),
		"font-styles" => false
	),
	"std" => array(
		"font-size" => '26',
		"font-family" => 'Playfair Display'
	),
	"type" => "typography"
);
$ipanel_himmelen_option[] = array(
	"name" => "Headers font parameters for Google Font",
	"id" => "header_font_options",
	"std" => "400,400italic,700,700italic",
	"desc" => "You can specify additional Google Fonts paramaters here, for example fonts styles to load. Default: 400",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Body font",
	"id" => "body_font",
	"desc" => "Font used in text elements. Default: Lato",
	"options" => array(
		"font-sizes" => array(
			" " => "Font Size",
			'11' => '11px',
			'12' => '12px',
			'13' => '13px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			'17' => '17px',
			'18' => '18px',
			'19' => '19px',
			'20' => '20px',
			'21' => '21px',
			'22' => '22px',
			'23' => '23px',
			'24' => '24px',
			'25' => '25px',
			'26' => '26px',
			'27' => '27px'
		),
		"color" => false,
		"font-families" => iPanel::getGoogleFonts(),
		"font-styles" => false
	),
	"std" => array(
		"font-size" => '15',
		"font-family" => 'Lato'
	),
	"type" => "typography"
);
$ipanel_himmelen_option[] = array(
	"name" => "Body font parameters for Google Font",
	"id" => "body_font_options",
	"std" => "400,400italic,700,700italic",
	"desc" => "You can specify additional Google Fonts paramaters here, for example fonts styles to load. Default: 400,400italic,700,700italic",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Additional font",
	"id" => "additional_font",
	"desc" => "You can select any additional Google font here and use it in Custom CSS in theme. Default: Montserrat",
	"options" => array(
		"font-sizes" => array(
			" " => "Font Size",
			'11' => '11px',
			'12' => '12px',
			'13' => '13px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			'17' => '17px',
			'18' => '18px',
			'19' => '19px',
			'20' => '20px',
			'21' => '21px',
			'22' => '22px',
			'23' => '23px',
			'24' => '24px',
			'25' => '25px',
			'26' => '26px',
			'27' => '27px',
			'28' => '28px',
			'29' => '29px',
			'30' => '30px',
			'31' => '31px',
			'32' => '32px',
			'33' => '33px',
			'34' => '34px',
			'35' => '35px',
			'36' => '36px',
			'37' => '37px',
			'38' => '38px',
			'39' => '39px',
			'40' => '40px',
			'41' => '41px',
			'42' => '42px',
			'43' => '43px',
			'44' => '44px',
			'45' => '45px',
			'46' => '46px',
			'47' => '47px',
			'48' => '48px',
			'49' => '49px',
			'50' => '50px'
		),
		"color" => false,
		"font-families" => iPanel::getGoogleFonts(),
		"font-styles" => false
	),
	"std" => array(
		"font-size" => '12',
		"font-family" => 'Montserrat'
	),
	"type" => "typography"
);
$ipanel_himmelen_option[] = array(
	"name" => "Additional font parameters for Google Font",
	"id" => "additional_font_options",
	"std" => "400",
	"desc" => "You can specify additional Google Fonts paramaters here, for example fonts styles to load. Default: 400,400italic,700,700italic",
	"type" => "text",
);
$ipanel_himmelen_option[] = array(
	"name" => "Enable Additional font",
	"id" => "additional_font_enable",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Uncheck if you don't want to use Additional font. This will speed up your site.",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "<span style='color:red'>Disable ALL Google Fonts on site</span>",
	"id" => "font_google_disable",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Use this if you want extra site speed or want to have regular fonts. Arial font will be used with this option.",
	"type" => "checkbox",
);
$ipanel_himmelen_option[] = array(
	"name" => "Regular font (apply if you disabled Google Fonts below)",
	"id" => "font_regular",
	"std" => "Arial",
	"options" => array(
		"Arial" => "Arial",
		"Tahoma" => "Tahoma",
		"Times New Roman" => "Times New Roman",
		"Verdana" => "Verdana",
		"Helvetica" => "Helvetica",
		"Georgia" => "Georgia",
		"Courier New" => "Courier New"
	),
	"desc" => "Use this option if you disabled ALL Google Fonts.",
	"type" => "select",
);
$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);

/**
 * COLORS TAB
 **/

$ipanel_himmelen_tabs[] = array(
	'name' => 'Colors & Skins',
	'id' => 'color_settings'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "color_settings",
);
$ipanel_himmelen_option[] = array(
	"name" => "Predefined color skins",
	"id" => "color_skin_name",
	"std" => "none",
	"options" => array(
		"none" => "Use colors specified below",
		"default" => "Himmelen (Default)",
		"black" => "Black",
		"grey" => "Grey",
		"lightblue" => "Light blue",
		"blue" => "Blue",
		"red" => "Red",
		"green" => "Green",
		"magnium" => "Magnium",
		"fencer" => "Fencer",
		"perfectum" => "Perfectum",
		"simplegreat" => "Simplegreat",
		"piemont" => "Piemont",
	),
	"desc" => "Select one of predefined skins or use your own colors. If you selected any predefined styles your specified colors below will NOT be applied.",
	"type" => "select",
);

$ipanel_himmelen_option[] = array(
	"name" => "Body background color",
	"id" => "theme_body_color",
	"std" => "#F7F7F7",
	"desc" => "Used in many theme places, default: #F7F7F7",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_himmelen_option[] = array(
	"name" => "Body text color",
	"id" => "theme_text_color",
	"std" => "#000000",
	"desc" => "Body text color, default: #000000",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_himmelen_option[] = array(
	"name" => "Theme main color",
	"id" => "theme_main_color",
	"std" => "#9BA78A",
	"desc" => "Used in many theme places, buttons, links, etc. Default: #9BA78A",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_himmelen_option[] = array(
	"name" => "Header background color",
	"id" => "theme_header_bg_color",
	"std" => "#FFFFFF",
	"desc" => "Default: #FFFFFF",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_himmelen_option[] = array(
	"name" => "Category menu background color",
	"id" => "theme_cat_menu_bg_color",
	"std" => "#FFFFFF",
	"desc" => "This background will be used for main menu below header. Default: #FFFFFF",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_himmelen_option[] = array(
	"name" => "Footer background color",
	"id" => "theme_footer_color",
	"std" => "#3c3d41",
	"desc" => "Default: #3c3d41",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_himmelen_option[] = array(
	"name" => "Masonry/List blog layout blocks background color",
	"id" => "theme_masonry_bg_color",
	"std" => "#FFFFFF",
	"desc" => "Default: #FFFFFF",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);

/**
 * CUSTOM CODE TAB
 **/

$ipanel_himmelen_tabs[] = array(
	'name' => 'Custom code',
	'id' => 'custom_code'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "custom_code",
);

$ipanel_himmelen_option[] = array(
	"name" => "Custom JavaScript code",
	"id" => "custom_js_code",
	"std" => '',
	"field_options" => array(
		"language" => "javascript",
		"line_numbers" => true,
		"autoCloseBrackets" => true,
		"autoCloseTags" => true
	),
	"desc" => "This code will run in header",
	"type" => "code",
);

$ipanel_himmelen_option[] = array(
	"name" => "Custom CSS styles",
	"id" => "custom_css_code",
	"std" => '',
	"field_options" => array(
		"language" => "json",
		"line_numbers" => true,
		"autoCloseBrackets" => true,
		"autoCloseTags" => true
	),
	"desc" => "This CSS code will be included in header",
	"type" => "code",
);

$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);

/**
 * DOCUMENTATION TAB
 **/

$ipanel_himmelen_tabs[] = array(
	'name' => 'Documentation',
	'id' => 'documentation'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "documentation"
);

function get_plugin_version_number($plugin_name) {
        // If get_plugins() isn't available, require it
	if ( ! function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
        // Create the plugins folder and file variables
	$plugin_folder = get_plugins( '/' . $plugin_name );
	$plugin_file = $plugin_name.'.php';
	
	// If the plugin version number is set, return it 
	if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) {
		return $plugin_folder[$plugin_file]['Version'];

	} else {
	// Otherwise return null
		return 'Plugin not installed';
	}
}

$ipanel_himmelen_option[] = array(
	"type" => "htmlpage",
	"name" => '<div class="documentation-icon"><img src="'.IPANEL_URI . 'assets/img/documentation-icon.png" alt="Documentation"/></div><p>We recommend you to read <a href="http://magniumthemes.com/go/himmelen-docs/" target="_blank">Theme Documentation</a> before you will start using our theme to building your website. It covers all steps for site configuration, demo content import, theme features usage and more.</p>
<p>If you have face any problems with our theme feel free to use our <a href="http://support.magniumthemes.com/" target="_blank">Support System</a> to contact us and get help for free.</p>
<a class="button button-primary" href="http://magniumthemes.com/go/himmelen-docs/" target="_blank">Theme Documentation</a>
<a class="button button-primary" href="http://support.magniumthemes.com/" target="_blank">Support System</a><h3>Technical information (paste it to your support ticket):</h3><textarea style="width: 500px; height: 160px;font-size: 12px;">Theme Version: '.wp_get_theme()->get( 'Version' ).'
WordPress Version: '.get_bloginfo( 'version' ).'
Admin Panel Access: '.get_admin_url().'
Admin Panel User login: [ADD_YOUR_LOGIN_HERE]
Admin Panel User password: [ADD_YOUR_PASSWORD_HERE]</textarea>'
);

$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);

/**
 * EXPORT TAB
 **/

$ipanel_himmelen_tabs[] = array(
	'name' => 'Export',
	'id' => 'export_settings'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "export_settings"
);
	
$ipanel_himmelen_option[] = array(
	"name" => "Export with Download Possibility",
	"type" => "export",
	"desc" => "Export theme admin panel settings to file."
);

$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);

/**
 * IMPORT TAB
 **/

$ipanel_himmelen_tabs[] = array(
	'name' => 'Import',
	'id' => 'import_settings'
);

$ipanel_himmelen_option[] = array(
	"type" => "StartTab",
	"id" => "import_settings"
);

$ipanel_himmelen_option[] = array(
	"name" => "Import",
	"type" => "import",
	"desc" => "Select theme options import file or paste options string to import your settings from Export."
);

$ipanel_himmelen_option[] = array(
	"type" => "EndTab"
);

/**
 * CONFIGURATION
 **/

$ipanel_configs = array(
	'ID'=> 'HIMMELEN_PANEL', 
	'menu'=> 
		array(
			'submenu' => false,
			'page_title' => __('Himmelen Control Panel', 'himmelen'),
			'menu_title' => __('Himmelen Control Panel', 'himmelen'),
			'capability' => 'manage_options',
			'menu_slug' => 'manage_theme_options',
			'icon_url' => IPANEL_URI . 'assets/img/panel-icon.png',
			'position' => 59
		),
	'rtl' => ( function_exists('is_rtl') && is_rtl() ),
	'tabs' => $ipanel_himmelen_tabs,
	'fields' => $ipanel_himmelen_option,
	'download_capability' => 'manage_options',
	'live_preview' => false
);

$ipanel_theme_usage = new IPANEL( $ipanel_configs );
	