<?php

	add_action( 'wp_enqueue_scripts', 'himmelen_enqueue_dynamic_js', '999' );

	function himmelen_enqueue_dynamic_js( ) {
		// remove later
		global $himmelen_theme_options;

		if ( function_exists( 'is_multisite' ) && is_multisite() ){
			$cache_file_name = 'cache.skin.b' . get_current_blog_id();
		} else {
			$cache_file_name = 'cache.skin';
		}

        $ipanel_saved_date = get_option( 'ipanel_saved_date', 1 );
        $cache_saved_date = get_option( 'cache_js_saved_date', 0 );

		if( file_exists( get_stylesheet_directory() . '/cache/' . $cache_file_name . '.js' ) ) {
			$cache_status = 'exist';

            if($ipanel_saved_date > $cache_saved_date) {
                $cache_status = 'no-exist';
            }

		} else {
			$cache_status = 'no-exist';
		}

        if ( defined('DEMO_MODE') ) {
            $cache_status = 'no-exist';
        }

		if ( $cache_status == 'exist' ) {

			wp_register_script( $cache_file_name, get_stylesheet_directory_uri() . '/cache/' . $cache_file_name . '.js',  array(), $cache_saved_date);
			wp_enqueue_script( $cache_file_name );

		} else {
			
			$out = '';

			$js_generated = microtime(true);

			$out = himmelen_get_js();

			$out .= '/* JS Generator Execution Time: ' . floatval( ( microtime(true) - $js_generated ) ) . ' seconds */';

			$cache_file = @fopen( get_stylesheet_directory() . '/cache/' . $cache_file_name . '.js', 'w' );

			if ( @fwrite( $cache_file, $out ) ) {

				wp_register_script($cache_file_name, get_template_directory_uri() . '/cache/' . $cache_file_name . '.js', array(), $cache_saved_date);
				wp_enqueue_script( $cache_file_name );

                // Update save options date
                $option_name = 'cache_js_saved_date';
                
                $new_value = microtime(true) ;

                if ( get_option( $option_name ) !== false ) {

                    // The option already exists, so we just update it.
                    update_option( $option_name, $new_value );

                } else {

                    // The option hasn't been added yet. We'll add it with $autoload set to 'no'.
                    $deprecated = null;
                    $autoload = 'no';
                    add_option( $option_name, $new_value, $deprecated, $autoload );
                }
			}
		
		}
	}

	function himmelen_get_js () {
		global $himmelen_theme_options;
		// ===
		ob_start();
    ?>
    (function($){
    $(document).ready(function() {
        
        
        <?php if(isset($himmelen_theme_options['custom_js_code'])) { 

        	echo $himmelen_theme_options['custom_js_code'];  // This variable contains user Custom JS code and can't be escaped with WordPress functions 

        } ?>

    });
    })(jQuery);
    <?php

    	$out = ob_get_clean();

		$out .= ' /*' . date("Y-m-d H:i") . '*/';
		/* RETURN */
		return $out;
	}

?>