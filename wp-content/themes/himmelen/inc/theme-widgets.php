<?php
/* Widgets */
global $himmelen_theme_options;

function himmelen_widgets_init() {
    global $himmelen_theme_options;
    
    register_sidebar(
      array(
        'name' => __( 'Left/Right sidebar', 'himmelen' ),
        'id' => 'main-sidebar',
        'description' => __( 'Widgets in this area will be shown in the left or right site column.', 'himmelen' )
      )
    );

    register_sidebar(
      array(
        'name' => __( 'Offcanvas Left sidebar', 'himmelen' ),
        'id' => 'offcanvas-sidebar',
        'description' => __( 'Widgets in this area will be shown in the left floating offcanvas menu sidebar that can be opened by toggle button in header. You can enable this sidebar in theme control panel.', 'himmelen' )
      )
    );

    register_sidebar(
      array(
        'name' => __( 'Footer light sidebar', 'himmelen' ),
        'id' => 'footer-sidebar',
        'description' => __( 'Widgets in this area will be shown in site footer in 4 column.', 'himmelen' )
      )
    );

    register_sidebar(
      array(
        'name' => __( 'Footer dark sidebar', 'himmelen' ),
        'id' => 'footer-sidebar-2',
        'description' => __( 'Widgets in this area will be shown in site footer in 5 column after Footer sidebar #1.', 'himmelen' )
      )
    );

    // Custom widgets
    register_widget('Himmelen_Widget_Recent_Posts');
    register_widget('Himmelen_Widget_Recent_Comments');
    register_widget('Himmelen_Widget_Content');
}

add_action( 'widgets_init', 'himmelen_widgets_init' );

// Allow shortcodes in widgets
add_filter('widget_text', 'do_shortcode');
add_filter('widget_himmelen_text', 'do_shortcode');

/* Custom widgets */

/**
 * Recent_Posts widget class
 */
class Himmelen_Widget_Recent_Posts extends WP_Widget {

    public function __construct() {
        $widget_ops = array('classname' => 'widget_himmelen_recent_entries', 'description' => __( "Your site&#8217;s most recent Posts with thumbnails.", 'himmelen') );
        parent::__construct('himmelen-recent-posts', __('Himmelen Recent Posts', 'himmelen'), $widget_ops);
        $this->alt_option_name = 'widget_himmelen_recent_entries';

        add_action( 'save_post', array($this, 'flush_widget_cache') );
        add_action( 'deleted_post', array($this, 'flush_widget_cache') );
        add_action( 'switch_theme', array($this, 'flush_widget_cache') );
    }

    public function widget($args, $instance) {
        $cache = array();
        if ( ! $this->is_preview() ) {
            $cache = wp_cache_get( 'widget_himmelen_recent_posts', 'widget' );
        }

        if ( ! is_array( $cache ) ) {
            $cache = array();
        }

        if ( ! isset( $args['widget_id'] ) ) {
            $args['widget_id'] = $this->id;
        }

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        ob_start();

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts', 'himmelen' );

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;
        $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;
        $show_thumb = isset( $instance['show_thumb'] ) ? $instance['show_thumb'] : false;

        /**
         * Filter the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         */
        $r = new WP_Query( apply_filters( 'widget_himmelen_posts_args', array(
            'posts_per_page'      => $number,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true
        ) ) );

        if ($r->have_posts()) :
?>
        <?php echo $args['before_widget']; ?>
        <?php if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        } ?>
        <ul>
        <?php while ( $r->have_posts() ) : $r->the_post(); ?>
            <li class="clearfix">
            <?php if ( $show_thumb ) : ?>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-thumb-widget'); ?></a>
            <?php endif; ?>
            <div class="widget-post-details-wrapper">
                <a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
            <?php if ( $show_date ) : ?>
                <span class="post-date"><?php echo get_the_date(); ?></span>
            <?php endif; ?>
            </div>
            </li>
        <?php endwhile; ?>
        </ul>
        <?php echo $args['after_widget']; ?>
<?php
        // Reset the global $the_post as this query will have stomped on it
        wp_reset_postdata();

        endif;

        if ( ! $this->is_preview() ) {
            $cache[ $args['widget_id'] ] = ob_get_flush();
            wp_cache_set( 'widget_himmelen_recent_posts', $cache, 'widget' );
        } else {
            ob_end_flush();
        }
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = (int) $new_instance['number'];
        $instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
        $instance['show_thumb'] = isset( $new_instance['show_thumb'] ) ? (bool) $new_instance['show_thumb'] : false;
        $this->flush_widget_cache();

        $alloptions = wp_cache_get( 'alloptions', 'options' );
        if ( isset($alloptions['widget_himmelen_recent_entries']) )
            delete_option('widget_himmelen_recent_entries');

        return $instance;
    }

    public function flush_widget_cache() {
        wp_cache_delete('widget_pieomnt_recent_posts', 'widget');
    }

    public function form( $instance ) {
        $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
        $show_thumb = isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : false;
?>
        <p><label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e( 'Title:', 'himmelen' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <p><label for="<?php echo esc_attr($this->get_field_id( 'number' )); ?>"><?php _e( 'Number of posts to show:', 'himmelen' ); ?></label>
        <input id="<?php echo esc_attr($this->get_field_id( 'number' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'number' )); ?>" type="text" value="<?php echo esc_attr($number); ?>" size="3" /></p>

        <p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo esc_attr($this->get_field_id( 'show_date' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_date' )); ?>" />
        <label for="<?php echo esc_attr($this->get_field_id( 'show_date' )); ?>"><?php _e( 'Display post date?', 'himmelen' ); ?></label></p>

        <p><input class="checkbox" type="checkbox" <?php checked( $show_thumb ); ?> id="<?php echo esc_attr($this->get_field_id( 'show_thumb' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_thumb' )); ?>" />
        <label for="<?php echo esc_attr($this->get_field_id( 'show_thumb' )); ?>"><?php _e( 'Display post featured image?', 'himmelen' ); ?></label></p>
<?php
    }
}

/**
 * Recent_Comments widget class
 *
 */
class Himmelen_Widget_Recent_Comments extends WP_Widget {

    public function __construct() {
        $widget_ops = array('classname' => 'widget_himmelen_recent_comments', 'description' => __( 'Your site&#8217;s most recent comments with date.', 'himmelen' ) );
        parent::__construct('himmelen-recent-comments', __('Himmelen Recent Comments', 'himmelen'), $widget_ops);
        $this->alt_option_name = 'widget_himmelen_recent_comments';

        add_action( 'comment_post', array($this, 'flush_widget_cache') );
        add_action( 'edit_comment', array($this, 'flush_widget_cache') );
        add_action( 'transition_comment_status', array($this, 'flush_widget_cache') );
    }

    public function flush_widget_cache() {
        wp_cache_delete('widget_himmelen_recent_comments', 'widget');
    }

    public function widget( $args, $instance ) {
        global $comments, $comment;

        $cache = array();
        if ( ! $this->is_preview() ) {
            $cache = wp_cache_get('widget_himmelen_recent_comments', 'widget');
        }
        if ( ! is_array( $cache ) ) {
            $cache = array();
        }

        if ( ! isset( $args['widget_id'] ) )
            $args['widget_id'] = $this->id;

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        $output = '';

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Comments', 'himmelen' );

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;

        /**
         * Filter the arguments for the Recent Comments widget.
         *
         * @since 3.4.0
         *
         * @see WP_Comment_Query::query() for information on accepted arguments.
         *
         * @param array $comment_args An array of arguments used to retrieve the recent comments.
         */
        $comments = get_comments( apply_filters( 'widget_comments_args', array(
            'number'      => $number,
            'status'      => 'approve',
            'post_status' => 'publish'
        ) ) );

        $output .= $args['before_widget'];
        if ( $title ) {
            $output .= $args['before_title'] . $title . $args['after_title'];
        }

        $output .= '<ul id="himmelen_recentcomments">';
        if ( $comments ) {
            // Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
            $post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
            _prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

            foreach ( (array) $comments as $comment) {
                $output .= '<li class="himmelen_recentcomments">';
                /* translators: comments widget: 1: comment author, 2: post link */
                $output .= '<a href="' . esc_url( get_comment_link( $comment->comment_ID ) ) . '">' . get_the_title( $comment->comment_post_ID ) . '</a><span class="comment-date">'.get_the_date().'</span>';
                
                $output .= '</li>';
            }
        }
        $output .= '</ul>';
        $output .= $args['after_widget'];

        echo $output; // This variable contains wordpress widget code and can't be escaped with WordPress functions 

        if ( ! $this->is_preview() ) {
            $cache[ $args['widget_id'] ] = $output;
            wp_cache_set( 'widget_himmelen_recent_comments', $cache, 'widget' );
        }
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = absint( $new_instance['number'] );
        $this->flush_widget_cache();

        $alloptions = wp_cache_get( 'alloptions', 'options' );
        if ( isset($alloptions['widget_himmelen_recent_comments']) )
            delete_option('widget_himmelen_recent_comments');

        return $instance;
    }

    public function form( $instance ) {
        $title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
?>
        <p><label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e( 'Title:', 'himmelen' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <p><label for="<?php echo esc_attr($this->get_field_id( 'number' )); ?>"><?php _e( 'Number of comments to show:', 'himmelen' ); ?></label>
        <input id="<?php echo esc_attr($this->get_field_id( 'number' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'number' )); ?>" type="text" value="<?php echo esc_attr($number); ?>" size="3" /></p>
<?php
    }
}

/**
 * Himmelen Content widget class
 *
 * @since 2.8.0
 */
class Himmelen_Widget_Content extends WP_Widget {

    public function __construct() {
        $widget_ops = array('classname' => 'widget_himmelen_text', 'description' => __('Add widget with any HTML content or shortcodes inside.', 'himmelen'));
        $control_ops = array('width' => 400, 'height' => 350);
        parent::__construct('himmelen-text', __('Himmelen Content', 'himmelen'), $widget_ops, $control_ops);
    }

    /**
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        /**
         * Filter the content of the Text widget.
         *
         * @since 2.3.0
         *
         * @param string    $widget_text The widget content.
         * @param WP_Widget $instance    WP_Widget instance.
         */
        $text = apply_filters( 'widget_himmelen_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );
        echo $args['before_widget'];

        ?>
        <div class="himmelen-textwidget-wrapper <?php echo !empty( $instance['paddings'] ) ? ' himmelen-textwidget-no-paddings' : ''; ?>">
        <?php
        if ( ! empty( $title ) ) {
            echo $args['before_title'] . $title . $args['after_title'];
        } 
        if(!empty( $instance['button_target'])) {
            $button_target = '_blank';
        } else {
            $button_target = '_self';
        }
        if(!empty( $instance['bg_image'])) {
            $style = 'background-image: url('.esc_url($instance['bg_image']).');';
        } else {
            $style = '';
        }

        if(!empty( $instance['custom_padding'])) {
            $style .= 'padding: '.esc_attr($instance['custom_padding']).';';
        }

        if(!empty( $instance['text_color'])) {
            $style .= 'color: '.esc_attr($instance['text_color']).';';
        }

        if(!empty( $instance['text_align'])) {
            $style .= 'text-align: '.esc_attr($instance['text_align']).';';
        }
        
        ?>
            <div class="himmelen-textwidget" data-style="<?php echo $style; ?>"><?php echo !empty( $instance['filter'] ) ? wpautop( $text ) : $text; ?><?php echo !empty( $instance['button_text'] ) ? '<a class="btn alt" href="'.esc_url($instance['button_url']).'" target="'.esc_attr($button_target).'">'.esc_html($instance['button_text']).'</a>' : ''; ?></div>
        </div>
        <?php
        echo $args['after_widget'];
    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['custom_padding'] = strip_tags($new_instance['custom_padding']);
        $instance['text_color'] = strip_tags($new_instance['text_color']);
        $instance['text_align'] = strip_tags($new_instance['text_align']);
        $instance['button_text'] = strip_tags($new_instance['button_text']);
        $instance['button_url'] = strip_tags($new_instance['button_url']);
        $instance['bg_image'] = strip_tags($new_instance['bg_image']);
        
        if ( current_user_can('unfiltered_html') )
            $instance['text'] =  $new_instance['text'];
        else
            $instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text']) ) ); // wp_filter_post_kses() expects slashed
        $instance['filter'] = ! empty( $new_instance['filter'] );
        $instance['paddings'] = ! empty( $new_instance['paddings'] );
        $instance['button_target'] = ! empty( $new_instance['button_target'] );

        return $instance;
    }

    /**
     * @param array $instance
     */
    public function form( $instance ) {

        wp_enqueue_media();
        wp_enqueue_style( 'wp-color-picker' ); 
        wp_enqueue_script( 'wp-color-picker' ); 

        $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '',  'button_text' => '','bg_image' => '','button_url' => '', 'custom_padding' => '', 'text_color' => '', 'text_align' => '') );
        $title = strip_tags($instance['title']);
        $button_text = strip_tags($instance['button_text']);
        $custom_padding = strip_tags($instance['custom_padding']);
        $bg_image = strip_tags($instance['bg_image']);
        $text_color = strip_tags($instance['text_color']);
        $text_align = strip_tags($instance['text_align']);
        
        $button_url = strip_tags($instance['button_url']);
        $text = esc_textarea($instance['text']);
?> 
        <p><input id="<?php echo $this->get_field_id('paddings'); ?>" name="<?php echo $this->get_field_name('paddings'); ?>" type="checkbox" <?php checked(isset($instance['paddings']) ? $instance['paddings'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('paddings'); ?>"><?php _e('Disable paddings in widget', 'himmelen'); ?></label></p>
        <p><label for="<?php echo $this->get_field_id('custom_padding'); ?>"><?php _e('Custom padding for content:', 'himmelen'); ?></label>
        <input class="" id="<?php echo $this->get_field_id('custom_padding'); ?>" name="<?php echo $this->get_field_name('custom_padding'); ?>" type="text" placeholder="<?php _e('For ex.: 10px 5px 10px 5px', 'himmelen'); ?>" value="<?php echo esc_attr($custom_padding); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('text_align'); ?>"><?php _e('Text align:', 'himmelen'); ?></label>
        <select id="<?php echo $this->get_field_id('text_align'); ?>" name="<?php echo $this->get_field_name('text_align'); ?>">
            <option value="<?php echo esc_attr($text_align); ?>" selected><?php echo esc_attr($text_align); ?></option>
            <option value="left"><?php _e('Left', 'himmelen'); ?></option>
            <option selected value="center"><?php _e('Center', 'himmelen'); ?></option>
            <option value="right"><?php _e('Right', 'himmelen'); ?></option>
        </select>
        </p><p><label class="label-text-color" for="<?php echo $this->get_field_id('text_color'); ?>"><?php _e('Text color:', 'himmelen'); ?></label>
        <input class="select-text-color" id="<?php echo $this->get_field_id('text_color'); ?>" name="<?php echo $this->get_field_name('text_color'); ?>" placeholder="<?php _e('For example: #ffffff', 'himmelen'); ?>" type="text" value="<?php echo esc_attr($text_color); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('bg_image'); ?>"><?php _e('Background image url:', 'himmelen'); ?></label><br/>
        <input class="" id="<?php echo $this->get_field_id('bg_image'); ?>" name="<?php echo $this->get_field_name('bg_image'); ?>" type="text" value="<?php echo esc_attr($bg_image); ?>" /><a class="button upload-widget-bg-image" data-input_id="<?php echo $this->get_field_id('bg_image'); ?>" data-uploader_button_text="<?php _e('Select background image', 'himmelen'); ?>" data-uploader_title="<?php _e('Add background image to widget', 'himmelen'); ?>"><?php _e( 'Select image', 'himmelen' ); ?></a></p>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title:', 'himmelen'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
     
        <p><label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Content:', 'himmelen' ); ?></label>
        <p><a class="button upload-widget-image" data-textarea_id="<?php echo $this->get_field_id('text'); ?>" data-uploader_button_text="Add image to content" data-uploader_title="Add image to widget content"><?php _e( 'Add Image to content', 'himmelen' ); ?></a></p>
        <textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>
        </p>
         <p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs', 'himmelen'); ?></label></p>

           <p><label for="<?php echo $this->get_field_id('button_text'); ?>"><?php _e('Button Text:', 'himmelen'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('button_text'); ?>" placeholder="<?php _e('Leave empty to disable button', 'himmelen'); ?>" name="<?php echo $this->get_field_name('button_text'); ?>" type="text" value="<?php echo esc_attr($button_text); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('button_url'); ?>"><?php _e('Button URL:', 'himmelen'); ?></label>
        <input class="" id="<?php echo $this->get_field_id('button_url'); ?>" name="<?php echo $this->get_field_name('button_url'); ?>" type="text" value="<?php echo esc_attr($button_url); ?>" /> <input id="<?php echo $this->get_field_id('button_target'); ?>" name="<?php echo $this->get_field_name('button_target'); ?>" type="checkbox" <?php checked(isset($instance['button_target']) ? $instance['button_target'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('button_target'); ?>"><?php _e('Open in new tab', 'himmelen'); ?></label></p>
    
       
<?php

    }
}

?>