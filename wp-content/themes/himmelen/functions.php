<?php
/**
 * Himmelen functions
 *
 * @package Himmelen
 */

/*
 *	@@@ iPanel Path Constant @@@
*/
define( 'IPANEL_PATH' , get_template_directory() . '/iPanel/' ); 

/*
 *	@@@ iPanel URI Constant @@@
*/
define( 'IPANEL_URI' , get_template_directory_uri() . '/iPanel/' );

/*
 *	@@@ Usage Constant @@@
*/
define( 'IPANEL_PLUGIN_USAGE' , false );


/*
 *	@@@ Include iPanel Main File @@@
*/
include_once IPANEL_PATH . 'iPanel.php';

global $himmelen_theme_options;

if(get_option('HIMMELEN_PANEL')) {

	$himmelen_theme_options = maybe_unserialize(get_option('HIMMELEN_PANEL'));

} else {

	$himmelen_theme_options = '';
	
}

if (!isset($content_width))
	$content_width = 810; /* pixels */

if (!function_exists('himmelen_setup')) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function himmelen_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Himmelen, use a find and replace
	 * to change 'himmelen' to the name of your theme in all the template files
	 */
	load_theme_textdomain('himmelen', get_template_directory() . '/languages');

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support('automatic-feed-links');

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support('post-thumbnails');

	/**
	 * Enable support for JetPack Infinite Scroll
	 *
	 * @link https://jetpack.me/support/infinite-scroll/
	 */
	add_theme_support( 'infinite-scroll', array(
	    'container' => 'content',
	    'footer' => 'page',
	) );

	/**
	 * Enable support for Title Tag
	 *
	 */
	function himmelen_theme_slug_setup() {
	   add_theme_support( 'title-tag' );
	}
	add_action( 'after_setup_theme', 'himmelen_theme_slug_setup' );

	/**
	 * Enable support for Logo
	 */
	add_theme_support( 'custom-header', array(
	    'default-image' =>  get_template_directory_uri() . '/img/logo.png',
            'width'         => 260,
            'flex-width'    => true,
            'flex-height'   => false,
            'header-text'   => false,
	));

	/**
	 *	Woocommerce support
	 */
	add_theme_support( 'woocommerce' );

	/**
	 * Theme resize image
	 */
	add_image_size( 'blog-thumb', 1140, 660, true);
	add_image_size( 'blog-thumb-vertical', 400, 380, true);
	add_image_size( 'blog-thumb-2column-sidebar', 409, 237, true);
	add_image_size( 'blog-thumb-widget', 90, 70, true);

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
            'primary' => __('Header Menu', 'himmelen'),
            'footer' => __('Footer Menu', 'himmelen'),
	) );
	/*
	* Change excerpt length
	*/
	function himmelen_new_excerpt_length($length) {
		global $himmelen_theme_options;

		if(isset($himmelen_theme_options['post_excerpt_legth'])) {
			$post_excerpt_length = $himmelen_theme_options['post_excerpt_legth'];
		} else {
			$post_excerpt_length = 18;
		}

		return $post_excerpt_length;
	}
	add_filter('excerpt_length', 'himmelen_new_excerpt_length');
	/**
	 * Enable support for Post Formats
	 */
	add_theme_support('post-formats', array('aside', 'image', 'gallery', 'video', 'audio', 'quote', 'link', 'status', 'chat'));
}
endif;
add_action('after_setup_theme', 'himmelen_setup');

/**
 * Enqueue scripts and styles
 */
function himmelen_scripts() {
	global $himmelen_theme_options;

	wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style( 'bootstrap' );

	wp_enqueue_style( 'himmelen-fonts', himmelen_google_fonts_url(), array(), '1.0' );

	wp_register_style('owl-main', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.css');
	wp_register_style('owl-theme', get_template_directory_uri() . '/js/owl-carousel/owl.theme.css');
	wp_enqueue_style( 'owl-main' );
	wp_enqueue_style( 'owl-theme' );

	wp_register_style('stylesheet', get_stylesheet_uri(), array(), '1.0.1', 'all');
	wp_enqueue_style( 'stylesheet' );

	wp_register_style('responsive', get_template_directory_uri() . '/responsive.css', '1.0.2', 'all');
	wp_enqueue_style( 'responsive' );

	if(isset($himmelen_theme_options['enable_theme_animations']) && $himmelen_theme_options['enable_theme_animations']) {
		wp_register_style('animations', get_template_directory_uri() . '/css/animations.css');
		wp_enqueue_style( 'animations' );
	}

	wp_register_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.css');
	wp_register_style('select2-mgt', get_template_directory_uri() . '/js/select2/select2.css');
	wp_register_style('offcanvasmenu', get_template_directory_uri() . '/css/offcanvasmenu.css');
	wp_register_style('nanoscroller', get_template_directory_uri() . '/css/nanoscroller.css');
	wp_register_style('swiper', get_template_directory_uri() . '/css/idangerous.swiper.css');

	wp_enqueue_style( 'font-awesome' );
	wp_enqueue_style( 'select2-mgt' );
	wp_enqueue_style( 'offcanvasmenu' );
	wp_enqueue_style( 'nanoscroller' );
	wp_enqueue_style( 'swiper' );

	add_thickbox();
	
	wp_register_script('himmelen-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.1.1', true);
	wp_register_script('himmelen-easing', get_template_directory_uri() . '/js/easing.js', array(), '1.3', true);
	wp_register_script('himmelen-template', get_template_directory_uri() . '/js/template.js', array(), '1.0.1', true);
	wp_register_script('himmelen-select2', get_template_directory_uri() . '/js/select2/select2.min.js', array(), '3.5.1', true);
	wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.min.js', array(), '1.3.3', true);
	wp_register_script('nanoscroller', get_template_directory_uri() . '/js/jquery.nanoscroller.min.js', array(), '3.4.0', true);

	wp_enqueue_script('himmelen-script', get_template_directory_uri() . '/js/template.js', array('jquery', 'himmelen-bootstrap', 'himmelen-easing', 'himmelen-select2', 'owl-carousel', 'nanoscroller'), '1.0.1', true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

}
add_action('wp_enqueue_scripts', 'himmelen_scripts');

// Custom theme title
add_filter( 'wp_title', 'himmelen_wp_title', 10, 2 );

function himmelen_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'himmelen' ), max( $paged, $page ) );
	}

	return $title;
}

// Deregister scripts
function himmelen_dequeue_stylesandscripts() {
	if ( class_exists( 'woocommerce' ) ) {
		wp_dequeue_style( 'select2' );
		wp_deregister_style( 'select2' );
	} 
}
add_action( 'wp_enqueue_scripts', 'himmelen_dequeue_stylesandscripts', 100 );

/**
 * Enqueue scripts and styles for admin area
 */
function himmelen_admin_scripts() {
	wp_register_style( 'himmelen-style-admin', get_template_directory_uri() .'/css/admin.css' );
	wp_enqueue_style( 'himmelen-style-admin' );
	wp_register_style('font-awesome-admin', get_template_directory_uri() . '/css/font-awesome.css');
	wp_enqueue_style( 'font-awesome-admin' );

	wp_register_script('himmelen-template-admin', get_template_directory_uri() . '/js/template-admin.js', array(), '1.0', true);
	wp_enqueue_script('himmelen-template-admin');

}
add_action( 'admin_init', 'himmelen_admin_scripts' );

function himmelen_old_ie_fixes() {
    global $is_IE;
    if ( $is_IE ) {
        echo '<!--[if lt IE 9]>';
        echo '<script src="' . get_template_directory_uri() . '/js/html5shiv.js" type="text/javascript"></script>';
        echo '<![endif]-->';
    }
}
add_action( 'wp_head', 'himmelen_old_ie_fixes' );

function himmelen_load_wp_media_files() {
  wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'himmelen_load_wp_media_files' );

/**
 * Theme Welcome message
 */
function himmelen_show_admin_notice() {
	global $current_user;
	$user_id = $current_user->ID;

	if ( ! get_user_meta($user_id, 'mgt_himmelen_welcome_message_ignore') && ( current_user_can( 'install_plugins' ) ) ):
    ?>
    <div class="updated mgt-welcome-message">
    <div class="mgt-welcome-message-show-steps"><div class="mgt-welcome-logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" style="height: 10px;" alt="<?php bloginfo('name'); ?>"></div><p class="about-description" style="display: inline-block;margin-bottom: 0; margin-top:3px;margin-right: 5px;">Follow this steps to setup your Himmelen theme within minutes</p> <a class="button button-primary" id="mgt-welcome-message-show-steps">Show steps</a> <a class="button button-secondary" href="<?php echo esc_url( add_query_arg( 'mgt_welcome_message_dismiss', '0' ) );?>">Hide this message forever</a></div>
    <div class="mgt-welcome-message-steps-wrapper">
    	<h2>Thanks for choosing Himmelen WordPress theme</h2>
        <p class="about-description">Follow this steps to setup your website within minutes:</p>
    	<div class="mgt-divider"><a href="<?php echo esc_url( add_query_arg( 'page', 'install-required-plugins', 'themes.php' ) ); ?>" class="button button-primary button-hero"><span class="button-step">1</span>Install required & recommended plugins</a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url( add_query_arg( 'page', 'radium_demo_installer', 'themes.php' ) ); ?>" class="button button-primary button-hero"><span class="button-step">2</span>Use 1-Click Demo Data Import</a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url( add_query_arg( 'page', 'ipanel_HIMMELEN_PANEL', 'admin.php' ) ); ?>" class="button button-primary button-hero"><span class="button-step">3</span>Manage theme options</a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url('http://magniumthemes.com/go/himmelen-docs/'); ?>" target="_blank" class="button button-secondary button-hero"><span class="button-step">4</span>Read Theme Documentation Guide</a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url('http://eepurl.com/WXNyr'); ?>" target="_blank" class="button button-secondary button-hero"><span class="button-step">5</span>Subscribe to updates</a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url('http://support.magniumthemes.com/'); ?>" target="_blank" class="button button-secondary button-hero"><span class="button-step">6</span>Ask for support if something does not work</a></div>
		<div class="mgt-divider"><a href="<?php echo esc_url('http://magniumthemes.com/how-to-rate-items-on-themeforest/'); ?>" target="_blank" class="button button-secondary button-hero"><span class="button-step">7</span>Rate our Theme if you enjoy it!</a><a id="mgt-dismiss-notice" class="button-secondary" href="<?php echo esc_url( add_query_arg( 'mgt_welcome_message_dismiss', '0' ) );?>">Hide this message</a></div>
    </div>
    </div>    	         
	<?php
	endif;
}
add_action( 'admin_notices', 'himmelen_show_admin_notice' );

function himmelen_welcome_message_dismiss() {
	global $current_user;
    $user_id = $current_user->ID;
    /* If user clicks to ignore the notice, add that to their user meta */
    if ( isset($_GET['mgt_welcome_message_dismiss']) && '0' == $_GET['mgt_welcome_message_dismiss'] ) {
	    add_user_meta($user_id, 'mgt_himmelen_welcome_message_ignore', 'true', true);
	}
}
add_action( 'admin_init', 'himmelen_welcome_message_dismiss' );

/**
 * Theme Update message
 */
function himmelen_show_admin_notice_update() {
	global $current_user;
	$user_id = $current_user->ID;

	if ( ! get_user_meta($user_id, 'mgt_himmelen_update_message_ignore') && ( current_user_can( 'install_plugins' ) ) ):
    ?>
    <div class="updated below-h2">
		<a href="<?php echo esc_url( add_query_arg( 'mgt_update_message_dismiss', '0' ) ); ?>" style="float: right;padding-top: 9px;">(never show this message again)&nbsp;&nbsp;<b>X</b></a><p style="display: inline-block;">Hi! Would you like to receive Himmelen theme updates news & get premium support? Subscribe to email notifications: </p>
		<form style="display: inline-block;" action="//magniumthemes.us8.list-manage.com/subscribe/post?u=6ff051d919df7a7fc1c84e4ad&amp;id=9285b358e7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		   <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your email">
		   <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
		</form>
    </div>
    <?php
	endif;
}
add_action( 'admin_notices', 'himmelen_show_admin_notice_update' );

function himmelen_update_message_dismiss() {
	global $current_user;
    $user_id = $current_user->ID;
    /* If user clicks to ignore the notice, add that to their user meta */
    if ( isset($_GET['mgt_update_message_dismiss']) && '0' == $_GET['mgt_update_message_dismiss'] ) {
	    add_user_meta($user_id, 'mgt_himmelen_update_message_ignore', 'true', true);
	}
}
add_action( 'admin_init', 'himmelen_update_message_dismiss' );


/**
 * Ajax registration PHP
 */
if (!function_exists('himmelen_registration_process_callback')) :
function himmelen_registration_process_callback() {
	$email = esc_html($_POST['email']);
	$code = esc_html($_POST['code']);

	echo $email.';'.$code.';'.get_option('admin_email').';'.wp_get_theme().';'.get_site_url();

	wp_die();
}
add_action('wp_ajax_himmelen_registration_process', 'himmelen_registration_process_callback');
endif;

/**
 * Ajax registration JS
 */
if (!function_exists('himmelen_registration_javascript')) :
function himmelen_registration_javascript() {
  ?>
  <script type="text/javascript" >
  (function($){
  $(document).ready(function($) {

	$('.theme-activation-wrapper .activate-theme-btn').on('click', function(e){

		var email = $('.theme-activation-wrapper .activate-theme-email').val();
		var code = $('.theme-activation-wrapper .activate-theme-code').val();

		if(email == '' || code == '') {
			$('.theme-activation-wrapper .theme-activation-message').html('<span class="error"><?php esc_html_e('Please fill out email and purchase code fields.', 'himmelen'); ?></span>');
		} else {
			$('.theme-activation-wrapper .activate-theme-btn').attr('disabled', 'disabled').removeClass('button-primary').addClass('button-secondary');

			$('.theme-activation-wrapper .theme-activation-message').html('<?php esc_html_e('Registering theme...', 'himmelen'); ?>');

			var data = {
		      action: 'himmelen_registration_process',
		      email: email,
		      code: code
		    };

			$.post( ajaxurl, data, function(response) {

		      var wpdata = response;

			  $.ajax({
			    url: "http://api.magniumthemes.com/activation.php?act=register&data="+wpdata,
			    type: "GET",
			    timeout: 10000,
			    success: function(data) { 
			    	if(data == 'verified') {
						
						$('.theme-activation-wrapper .theme-activation-message').html('<span class="success"><?php esc_html_e('Theme registered succesfully!', 'himmelen'); ?></span><br/><br>');

						window.location = "themes.php?page=ipanel_HIMMELEN_PANEL&act=registration_complete";


					} else {
						$('.theme-activation-wrapper .theme-activation-message').html('<span class="error"><?php esc_html_e('Purchase code is not valid. Your purchase code should look like this: 36434418-e837-48c5-8737-f20d52b36a1f', 'himmelen'); ?></span>');

						$('.theme-activation-wrapper .activate-theme-btn').removeAttr('disabled', 'disabled').removeClass('button-secondary').addClass('button-primary');

					}
			    },
			    error: function(xmlhttprequest, textstatus, message) {
			        $('.theme-activation-wrapper .theme-activation-message').html("<?php echo __(wp_kses_post("<span class='error'>Oops! It looks like the registration server is on technical maintenance.<br/>Please click the button below to start using all theme features right now. Don't worry, you can register your theme tomorrow.<br/>We're sorry for the inconvenience!<br/>If this issue persist on next activation this means your hosting blocks external connections to our server,<br/>please <a href='http://support.magniumthemes.com/' target='_blank'>contact our support team</a> to get theme activated manually.</span><br><a href='themes.php?page=ipanel_HIMMELEN_PANEL&act=registration_skip' class='button button-primary button-hero activate-theme-btn'>Start using theme</a>"), 'himmelen'); ?>");
			    }
			  });
		      	
		    });

	  	}

		
    });

  });
  })(jQuery);
  </script>
  <?php
}
add_action('admin_print_footer_scripts', 'himmelen_registration_javascript', 99);
endif;
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/theme-tags.php';

/**
 * Load theme functions.
 */
require get_template_directory() . '/inc/theme-functions.php';

/**
 * Load theme widgets.
 */
require get_template_directory() . '/inc/theme-widgets.php';

/**
 * Load theme dynamic CSS.
 */
require get_template_directory() . '/inc/theme-css.php';

/**
 * Load theme dynamic JS.
 */
require get_template_directory() . '/inc/theme-js.php';

/**
 * Load theme metaboxes.
 */
require get_template_directory() . '/inc/theme-metaboxes.php';

/**
 * Load one click demo import.
 */
global $pagenow;

if (( $pagenow !== 'admin-ajax.php' ) && (is_admin())) {
	require get_template_directory() .'/inc/oneclick-demo-import/init.php';
}