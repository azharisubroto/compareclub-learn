<?php
/*
*	Related posts
*/
?>
<?php
global $post;

?>
<?php
//for use in the loop, list 5 post titles related to first tag on current post
$tags = wp_get_post_tags($post->ID);

if ($tags) {

	$intags = array();

	foreach ($tags as $tag) {
		$intags[] = $tag->term_id;
	}

	$args=array(
	'tag__in' => $intags,
	'post__not_in' => array($post->ID),
	'posts_per_page'=> 3
	);

	$my_query = new WP_Query($args);

	if( $my_query->have_posts() ) {

		echo '<div class="blog-post-related blog-post-related-single clearfix">';
		echo '<h5>'.__('You may also like','himmelen').'</h5>';

		while ($my_query->have_posts()) : $my_query->the_post(); 
			$post_image_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'blog-thumb');
			$post_image = $post_image_data[0];
		?>
		<div class="blog-post-related-item">

		<a href="<?php the_permalink() ?>" class="blog-post-related-image"><img src="<?php echo esc_attr($post_image); ?>" alt="<?php the_title_attribute(); ?>"/></a>
		<div class="blog-post-related-item-details">
			<?php
			$categories_list = get_the_category_list(  ', '  );
			if ( $categories_list ) :
			?>
		    <div class="post-categories"><?php printf( __( '%1$s', 'himmelen' ), $categories_list ); ?></div>
		    <?php endif; // End if categories ?>
			<a href="<?php the_permalink() ?>" class="blog-post-related-title"><?php the_title(); ?></a>
			<div class="blog-post-related-date"><?php echo get_the_time( get_option( 'date_format' ), get_the_ID() );?></div>
		</div>
		</div>
		<?php
		endwhile;

		echo '</div>';

	}

	wp_reset_query();

}

?>
